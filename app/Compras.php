<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compras extends Model
{
    //
    protected $table = 'compras';
    public $timestamps = false;

    protected $primaryKey = 'id';

    protected $fillable = [
        'fecha_compra',
        'monto',
        'folio',
        'users_id',
        'direccion_usuario_id'
     ];

    //funcion del many to one de compra_status
    public function compra_status(){
        //hasMany(nombre_clase.class)
        return $this->belongsTo('App\Http\Models\CompraEstatus','compra_id','id');
    }
    public function compra_producto(){
        return $this->hasMany('App\Http\Models\CompraProducto', 'compra_id', 'id')
    }
}

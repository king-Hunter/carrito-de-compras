<?php

namespace App\Http\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Carrito extends Model
{
    
    protected $table = 'carrito';
    protected $fillable = ['fecha','cantidad', 'users_id','producto_id'];
    public $timestamps = false;
    
}

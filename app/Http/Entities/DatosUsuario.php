<?php

namespace App\Http\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;

class DatosUsuario extends Model
{
    //
    protected $table = 'datos_usuario';
    protected $fillable = ['nombre','paterno', 'materno','telefono','users_id'];
    public $timestamps = false;
    
    /*public function users(){
        return $this->hasOne('App\User', 'id' ,'users_id')->get();
    }*/
}

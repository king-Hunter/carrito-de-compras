<?php

namespace App\Http\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Direccion extends Model
{
    //
    protected $table = 'direccion_usuario';
    protected $fillable = ['calle','numero_ext', 'numero_int','referencia','colonia','municipio','estado','codigo_postal','datos_usuario_id','status'];
    public $timestamps = false;
}

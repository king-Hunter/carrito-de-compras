<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class CatalogoTipoProductoModel extends Model
{
    protected $table = "cat_tipo_producto";
    public $timestamps = false;

    protected $fillable = [
        'nombre'
    ];


}
<?php

namespace App\Http\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Http\Entities\DatosUsuario;
use App\Http\Entities\Direccion;
use App\User;
use Illuminate\Database\Events\TransactionBeginning;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Boolean;

class UsuarioModel extends Model
{

    public function getAll()
    {
        return DatosUsuario::get();
    }
    public function showOne($id)
    {
        $data = $this->userData($id);
        if (!$data->tipo_usuario){
            $data->direccion = $this->numDirecUser($data);
        } else{
            $data->direccion = $this->direcionesNum();
        }
        if ($data->full_name == null && $data->telefono == null) {
            $data->dataCampos = "Este usuario aun no configura su perfil";
            $data = $this->verificar($data);
        } else {
            $data = $this->verificar($data);
        }
        
        return $data;
    }
    public function showPerfil($id)
    {
        return DatosUsuario::find($id);
    }
    public function verificar($data)
    {
        if ($data->full_name == null || $data->full_name == "") {
            $data->full_name = "Nombre aun no configurado";
        }

        if ($data->telefono == null || $data->telefono == "") {
            $data->telefono = "Telefono aun no configurado";
        }
        return $data;
    }
    public function guardar($request, $id)
    {
        try {
            $data = DatosUsuario::find($id)->update($request->all());
            $buscar = DatosUsuario::find($id);
            if ($data) {
                $data = $this->showOne($buscar->users_id);
            }
        } catch (\Throwable $th) {
            $data = $th;
        }
        return $data->users_id;
    }

    public function crear($request)
    {
        try {
            DatosUsuario::create($request->all());
            $data = true;
        } catch (\Throwable $th) {
            $data = false;
        }
        return $data;
    }
    public function borrar($request)
    {
        try {
            DatosUsuario::find($request)->delete();
            $data = true;
        } catch (\Throwable $th) {
            $data = false;
        }
        return $data;
    }
    public function direcciones()
    {
        try {
            $cantidad = $this->direcionesNum();
                $user = $this->userData(Auth::user()->id);
                $datos = $this->getDireccionConTipo($user);
                $data = Direccion::where($datos)->get();
            $data->cantidad = $cantidad;
        } catch (\Throwable $th) {
            return array($th);
        }
        return $data;
    }
    public function userDireccion($id){
        try {
            $data = $this->userData($id);
            $cantidad = $this->numDirecUser($data);
            $datos = $this->getDireccionConTipo($data);
                $data = Direccion::where($datos)->get();
                $data->cantidad = $cantidad;
        } catch (\Throwable $th) {
            return null;
        }
        return $data;
    }
    public function getDireccionConTipo($object){

        if (Auth::user()->tipo_usuario){
            $getdirec = [
                'datos_usuario_id' => $object->id,
                'status' => 0
            ];
        } else{
            $getdirec = [
                'datos_usuario_id' => $object->id,
                'status' => 1
            ];
            
        }
        return $getdirec;
    }
public function numDirecUser($data){
    try {
        $datos = $this->getDireccionConTipo($data);
        return Direccion::where($datos)->get()->count();
    } catch (\Throwable $th) {
        return null;
    }
}
    public function direcionesNum()
    {
        try {
            $data = $this->userData(Auth::user()->id);
            $datos = $this->getDireccionConTipo($data);
            return Direccion::where($datos)->get()->count();
        } catch (\Throwable $th) {
            return null;
        }
    }
    public function userData($id)
    {
        return DatosUsuario::select(
            'datos_usuario.id',
            DB::raw('CONCAT(nombre," ",paterno," ",materno) AS full_name'),
            'telefono',
            'users_id',
            'email',
            'tipo_usuario'
        )
            ->where('users_id', $id)->join('users', 'users.id', 'users_id')->first();
    }
    public function deleteAdrees($id){
        try {
            $data = Direccion::find($id);
            $data->status = '0';
            $data->save();
            if (!$data->status) {
                $code = 0;
            }
        } catch (\Throwable $th) {
            return $th;
        }
            return $code;
    }
    public function crearDorec($request){
        try {
            $item = $request->all();
            $item['status'] = 1;
            $temp = $this->userData(Auth::user()->id);
            $item['datos_usuario_id'] = $temp->id;
            Direccion::create($item);
            $data = true;
        } catch (\Throwable $th) {
            $data = $th;
        }
        return $data;   
    }
    public function buscarAdrees($request){
        return Direccion::find($request);
    }
    public function updateDirecciones($request, $id){
        try {

            $data = Direccion::find($id)->update($request->all());
        } catch (\Throwable $th) {
            $data = $th;
        }
        return $data;
    }
}

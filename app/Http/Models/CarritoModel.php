<?php

namespace App\Http\Models;

use App\Http\Entities\Carrito;
use Illuminate\Database\Eloquent\Model;
use App\Http\Entities\DatosUsuario;
use App\User;
use Illuminate\Database\Events\TransactionBeginning;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Boolean;
use Illuminate\Support\Facades\Auth;

class CarritoModel extends Model
{
    public function userData($id)
    {
        return DatosUsuario::select(
            'datos_usuario.id',
            DB::raw('CONCAT(nombre," ",paterno," ",materno) AS full_name'),
            'telefono',
            'users_id',
            'email',
            'tipo_usuario'
        )
            ->where('users_id', $id)->join('users', 'users.id', 'users_id')->first();
    }

    public function carritoList()
    {
        return  Carrito::where('users_id', Auth::user()->id)->join('producto as p', 'p.id', 'producto_id')
        ->select(
            'carrito.id',
            'cantidad',
            'users_id',
            'p.nombre',  
            'p.precio',
            'p.id as producto_id'
        )->get();
    }
    public static function cuantos() 
    {
        return  Carrito::where('users_id', Auth::user()->id)->join('producto', 'producto.id', 'producto_id')->get();
    }
    public function eliminarProduc($id)
    {
        try {
            Carrito::find($id)->delete();
            $data = true;
        } catch (\Throwable $th) {
            $data = false;
        }
        return $data;
    }
    public function updateProducto($value)
    {
        $s['fallo'] = true;
        $s['carrito'] = null;
        try {
            $data = Carrito::find($value->id);
            $data->cantidad = $value->cantidad;
            $data->save();
            $s['carrito'] = $this->carritoList();
        } catch (\Throwable $th) {
            $s['fallo'] = false;
        }
        return $s;
    }
    public function registrarCompra($data){
        $this->db->insertarRegistro('compra',$data);
    }
    public function obtenerCompraFolio($folio){
        $this->db->consulta("select * from compra c where c.folio = '$folio'");
        return $this->db->resultado_row();
    }
}

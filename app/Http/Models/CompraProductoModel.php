<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class CompraProductoModel extends Model
{
    protected $table = "compra_producto";
    public $timestamps = false;

    protected $fillable = [
        'precio_compra',
        'cantidad',
        'compra_id',
        'producto_id'
    ];
}

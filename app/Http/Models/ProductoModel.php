<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ProductoModel extends Model
{
    //Instanciar tabla
    protected $table = 'producto';
    public $timestamps = false;

    protected $primaryKey = 'id';


    protected $fillable = [
        'clave',
        'nombre',
        'descripcion',
        'precio',
        'cat_tipo_producto_id'
    ];

    public function imagenesProductos(){
        return $this->hasMany('App\Http\Models\ProductoGaleriaModel','producto_id','id');
    }

    public function categoriaProducto(){
        return $this->hasOne('App\Http\Models\CatalogoTipoProductoModel','id','cat_tipo_producto_id');
    }

}

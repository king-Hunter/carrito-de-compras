<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ComprasModel extends Model
{
    protected $table = 'compras';
    public $timestamps = false;

    protected $fillable = [
        'fecha_compra',
        'monto',
        'folio',
        'users_id',
        'direccion_usuario_id'
    ];
}

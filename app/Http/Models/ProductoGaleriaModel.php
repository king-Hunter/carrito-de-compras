<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ProductoGaleriaModel extends Model
{
    protected $table = "img_productos";
    public $timestamps = false;

    protected $fillable = [
        'nombre',
        'formato',
        'producto_id'
    ];

    public function producto(){
        return $this->belongsTo('app\Http\Models\ProductosModel');
    }

}

<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class CompraProducto extends Model
{
    //
    protected $table = 'compra_producto';
    public $timestamps = false;

    protected $fillable = [
        'id',
        'precio_compra',
        'cantidad',
        'compra_id',
        'producto_id'
    ];
    public function compras(){
        //hasMany(nombre_clase.class)
        return $this->belongsTo('App\Providers\Compras', 'id', 'compra_id');
    }
    public function productos(){
        return $this->hasOne('App\Http\Models\ProductoModel', 'id', 'producto_id');
    }

}

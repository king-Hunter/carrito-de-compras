<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class CompraEstatus extends Model
{
    protected $table = "compra_status";
    public $timestamps = false;

    protected $fillable = [
        'fecha',
        'compra_id',
        'nombre_status'
    ];

    public function compra(){
        return $this->hasMany('app\Compras');
    }
    
    public function crear($request){
        CompraEstatus::create($request->all());
        return $data;
    }

}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\CompraProducto;

class CompraProductoController extends Controller
{
    public function index()
    {
      //  $compra_producto = CompraProducto::orderBy('id','DESC')->paginate(3);
      //  return view('admin.compras.index', compact('compra_producto'));
        $datos['compra_producto'] = CompraProducto::join('compras', 'compras.id', 'compra_id')
        ->join('producto','producto.id.','producto_id')
        ->get();
    }
    public function show($id) 
    {
       $compra_producto = CompraProducto::where('compra_id',$id)
       ->get();

       // dd($compra_producto[0]->productos[0]->imagenesProductos);

       return view('Compras.showCompras', compact('compra_producto'));
        
    }

}

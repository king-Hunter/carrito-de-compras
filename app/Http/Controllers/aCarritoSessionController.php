<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\CarritoModel;
use App\Http\Models\UsuarioModel;
use Illuminate\Support\Facades\Session;

class aCarritoSessionController extends Controller
{
    public function __construct()
    {
        $this->usuario = new UsuarioModel();
        $this->carrito = new CarritoModel();
    }
    //

    public function carrito_cliente(Request $request){
        //dd($request->user_id);
        $usuario = $this->usuario->userData($request->user_id);
        $carrito = $this->carrito->carritoList($request->producto_id);
        
        //$request->session()->put('usuario', $usuario);
        Session::put('usuario.data',$usuario);
        Session::put('carrito.data', $carrito);
        Session::put('compra_monto', $request->compra_monto);
        
        echo json_encode($usuario);
      
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use App\Http\Models\ComprasModel;
use App\Http\Models\CompraEstatus;
use App\Http\Models\CarritoModel;
use App\Http\Models\CompraProductoModel;
use Illuminate\Support\Facades\Auth;
use App\Http\Entities\Carrito;
// use Paypal\Rest\ApiContext;
// use Paypal\Auth\OAuthTokenCredential;
use Illuminate\Support\Facades\Session;
use App\Http\Models\CompraProducto;

class PaymentController extends Controller
{

    private $apiContext;

    public function __construct()
    {
        $payPalConfig = Config::get('paypal');

        $this->apiContext =  new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                $payPalConfig['client_id'],
                $payPalConfig['secret']
            )
        );

        $this->apiContext->setConfig($payPalConfig['settings']);
    }
    public function folioRandom(){
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        return substr(str_shuffle($permitted_chars), 0, 10);
    }

    public function payWithPaypal(Request $request)
    {

        $monto = Session::get('compra_monto');
        $carrito = Session::get('carrito.data');
        $usuario = Session::get('usuario.data');
   
        $input = array(
            'id'=>'',
            'fecha_compra'=> now(),
            'monto'=>$monto,
            'folio'=>$this->folioRandom(),
            'users_id'=>$usuario->users_id,
            'direccion_usuario_id'=>$request->direccion_usuario_id
        );
       
        $compra = ComprasModel::create($input);
        Session::put('compra', $compra);

        
        // Registrar el detalle de la compra
        foreach($carrito as $detalle){
            $detalle_compra = array(
                'precio_compra'=>$detalle->precio,
                'cantidad'=>$detalle->cantidad,
                'compra_id'=>$compra->id,
                'producto_id'=>$detalle->producto_id
        
            );
        }
    
        // $monto = $this->getMontoCarrito();
        $payer = new \PayPal\Api\Payer();
        $payer->setPaymentMethod('paypal');

        $amount = new \PayPal\Api\Amount();
        $amount->setTotal($monto);
        $amount->setCurrency('MXN');

        $transaction = new \PayPal\Api\Transaction();
        $transaction->setAmount($amount);

        $callbackUrl = url('/paypal/status');
        $redirectUrls = new \PayPal\Api\RedirectUrls();
        $redirectUrls->setReturnUrl($callbackUrl)
            ->setCancelUrl($callbackUrl);

        $payment = new \PayPal\Api\Payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setTransactions(array($transaction))
            ->setRedirectUrls($redirectUrls);


            //Crear pago
            try {
                $payment->create($this->apiContext);
                // echo $payment;

                return redirect()->away($payment->getApprovalLink());
                // echo "\n\nRedirect user to approval_url: " . $payment->getApprovalLink() . "\n";
            }
            catch (\PayPal\Exception\PayPalConnectionException $ex) {
                // This will print the detailed information on the exception.
                //REALLY HELPFUL FOR DEBUGGING
                echo $ex->getData();
            }
    }

    public function payPalStatus(Request $request)
    {

        $compra_id = Session::get('compra.id');
        
        $paymentId = $request->input('paymentId');
        $payerId = $request->input('PayerID');
        $token = $request->input('token');

        if(!$paymentId || !$payerId || !$token){
            $status = "No se pudo proceder con el pago a través de PayPal";
            $compra_status = new CompraEstatus;
            $compra_status->compra_id = $compra_id;
            $compra_status->nombre_status = "No pagado";
            $compra_status->save();
            return redirect('/home')->with(compact('status'));
        }

        $payment = Payment::get($paymentId, $this->apiContext);

        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);

        /** Execute the payment **/
         $result = $payment->execute($execution, $this->apiContext);
         

        if ($result->getState() === 'approved') {
            $status = '¡Gracias! El pago a través de PayPal se ha ralizado correctamente.';
            

            // Guarda status de la compra
            $compra_status = new CompraEstatus;
            $compra_status->compra_id = $compra_id;
            $compra_status->nombre_status = "Pagado";
            $compra_status->save();

            //Registrar productos de la compra
            $carrito = Session::get('carrito.data');

            foreach($carrito as $registro){

                $insert_compra = new CompraProductoModel;
                $insert_compra->precio_compra = $registro->precio;
                $insert_compra->cantidad = $registro->cantidad;
                $insert_compra->compra_id = $compra_id;
                $insert_compra->producto_id = $registro->producto_id;
                $insert_compra->save();
            }
            // Eliminar del carrito

            $carritoEliminar = Carrito::where('users_id',Auth::user()->id)->delete();
            return redirect('/')->with(compact('status'));
        }

        $status = '¡Lo sentimos! El pago a través de PayPal no se pudo realizar.';

            $compra_status = new CompraEstatus;
            $compra_status->compra_id = $compra_id;
            $compra_status->nombre_status = "No pagado";
            $compra_status->save();

        return redirect('/home')->with(compact('status'));
    }

    //funcion para obtener el monto del carrito
    public function getCarritoUsuario(){
        
        $carritoUsuario = Carrito::where('users_id',Auth::user()->id)->get();
        
        
    }
}
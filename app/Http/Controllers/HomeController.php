<?php

namespace App\Http\Controllers;

use App\Http\Entities\Carrito;
use App\Http\Models\CarritoModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Models\ProductoModel;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    private $modelo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */    
    public function __construct()
    {
        $this->modelo = new CarritoModel();
        $this->middleware('auth');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos = ProductoModel::get();
       
        return view('welcome', compact('productos'));
    }
    public function carrito(){
        if (Auth::user()->tipo_usuario) {
            return view('admin');
        } else {
            $data = $this->modelo->carritoList();
            $direcciones = DB::table('datos_usuario as du')->join('direccion_usuario as diru','du.id','diru.datos_usuario_id')
            ->where('du.users_id',Auth::user()->id)
            ->select('diru.*')
            ->get();

            return view('client', compact('data','direcciones'));
        }
    }

    public function agregarProducto(Request $request)
    {
        $id_user = Auth::user()->id;
        $verificaCarrito = Carrito::where([['users_id',Auth::user()->id],['producto_id', $request->id_producto]])->first();

        if($verificaCarrito === null){
            
            $carrito = new Carrito;
            $carrito->cantidad = $request->cantidad;
            $carrito->users_id = $id_user;
            $carrito->producto_id = $request->id_producto;
            $carrito->save();
        } else{
            $cantidad = $verificaCarrito->cantidad;
            $verificaCarrito->cantidad = (int)$cantidad + (int)$request->cantidad;
            $verificaCarrito->save();
        }

        $return_json = [
            'success' => true,
            'id' => $request->id_producto,
            'cantidad' => $request->cantidad,
            'usuario' => $id_user,
        ];
        return $return_json;
    }

    // public function agregarProducto(Request $request)
    // {
    //     $id_user = Auth::user()->id;
        
    //     $verificaCarrito = Carrito::firstOrCreate(['users_id'=>$id_user],['producto_id'=> $request->id_producto]);
        
    //     $verificaCarrito->cantidad = $verificaCarrito->cantidad == null ? $request->cantidad : 
    //                                 (int)$verificaCarrito->cantidad + (int)$request->cantidad;
    //     $verificaCarrito->save();
    

    //     $return_json = [
    //         'success' => true,
    //         'id' => $request->id_producto,
    //         'cantidad' => $request->cantidad,
    //         'usuario' => $id_user,
    //     ];
    //     return $return_json;
    // }
    
}

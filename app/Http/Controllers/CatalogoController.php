<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\ProductoModel;

class CatalogoController extends Controller
{

    public function mostrarProductos()
    {
        $productos = ProductoModel::orderBy('id','DESC');
        
        return view('welcome', compact('productos'));
    }

}

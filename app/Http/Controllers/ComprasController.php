<?php

namespace App\Http\Controllers;

use App\Compras;
use App\Http\Models\CompraEstatus;
use App\Http\Models\ProductoModel;
use App\Http\Controllers\ProductoController;
use Illuminate\Http\Request;

class ComprasController extends Controller
{
      // Ver historial de compras
    public function verHistorial($id_user){
        $datos['compras'] = CompraEstatus::where('compras.users_id', $id_user)
        ->join('compras', 'compras.id', 'compra_id')
        ->join('direccion_usuario','compras.direccion_usuario_id','direccion_usuario.id')
        ->join('datos_usuario','compras.users_id','datos_usuario.id')
        ->get();

        return view('compras.index', $datos);

    }

    public function index()
    {
        $datos= $this->retornarDatos();
        return view('compras.index', $datos);
    }
    public function retornarDatos(){
        $datos['compras'] = CompraEstatus::join('compras', 'compras.id', 'compra_id')
        ->join('direccion_usuario','compras.direccion_usuario_id','direccion_usuario.id')
        ->join('datos_usuario','compras.users_id','datos_usuario.id')
        ->get();

        return $datos;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
       // dd($request->all());

       $data= CompraEstatus::create( $request->all() );
        $datos = $this->retornarDatos();
       //dd($datos[0]);
       return redirect()->route('compras.index', $datos)->with('success','Guardado');
       //return($request);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$name = $request->input('name');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Compras  $compras
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $productos = ProductoModel::find($id);

        // dd($productos->imagenesProductos());

        return view('admin.productos.show', compact('productos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Compras  $compras
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
      //  $compras = Compras::find($id);
      //  $data= CompraEstatus::edit( $request->all() );
      //  $datos = $this->retornarDatos();
       //dd($datos[0]);
       //return redirect()->route('compras.index', $datos)->with('success','Guardado');
      //  return view('admin.compras.index', compact('compras','compra_status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Compras  $compras
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        
        $compra_status = CompraEstatus::where('compra_id',$id)->first();
        $compra_status->nombre_status = $request->nombre_status;
        $compra_status->save();
        return redirect()->route('compras.index')->with('success','El estatus se actualizo correctamente');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Compras  $compras
     * @return \Illuminate\Http\Response
     */
    public function destroy(Compras $compras)
    {
        //
    }

  
}

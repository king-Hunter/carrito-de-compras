<?php

namespace App\Http\Controllers;

use App\Http\Models\CarritoModel;
use Illuminate\Http\Request;

class CarritoController extends Controller
{
    private $modelo;

    public function __construct()
    {
        $this->modelo = new CarritoModel();
    }

    public function enviar(Request $request)
    {
        Session::put('ejme', $request->compra_monto);
        dd($request->all());
        return false;
    }
    public function deleteProduc(Request $request)
    {
        $validate = $this->modelo->eliminarProduc($request->id);
        if ($validate == 0) {
            $return_json = [
                'success' => true,
                'msg' => 'Se elimino con exito'
            ];
        } else {
            $return_json = [
                'success' => true,
                'msg' => 'Error no se puedo eliminar'
            ];
        }
        return $return_json;
    }
    
    public function updateProduc(Request $request){
        
        $validate = $this->modelo->updateProducto($request);
        
        if ($validate['fallo']){
            $return_json = [
                'success' => true,
                'msg' => 'Se actualizo',
                'data' => $validate['carrito']
            ];
        } else {
            $return_json = [
                'success' => false,
                'msg' => 'Erro en el update',
                'data' => $validate['carrito']
            ];
        }
        return $return_json;
    }
}

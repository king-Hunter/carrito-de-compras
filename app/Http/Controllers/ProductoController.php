<?php

namespace App\Http\Controllers;

use App\Http\Models\ProductoModel;
use App\Http\Models\ProductoGaleriaModel;
use App\Http\Models\CatalogoTipoProductoModel;
use Illuminate\Http\Request;
use App\Http\Requests\ItemCreateRequest;
use App\Http\Requests\ItemUpdateRequest;
use DB;
use File; 
use Input;
use Storage; 
use Illuminate\Support\Str; 


class ProductoController extends Controller
{
    public function index()
    {
        $productos = ProductoModel::orderBy('id','DESC')->paginate(3);
        // $productos = ProductoModel::get();

        // dd($productos);

        return view('admin.productos.index', compact('productos'));
    }



    public function create()
    {
        $categorias = CatalogoTipoProductoModel::get();
        return view('admin.productos.create', compact('categorias'));
    }



    public function store(ItemCreateRequest $request)
    {
        $productos = new ProductoModel;
        $productos->clave = $request->clave;
        $productos->nombre = $request->nombre;
        $productos->descripcion = $request->descripcion;
        $productos->precio = $request->precio;
        $productos->cat_tipo_producto_id = $request->cat_tipo_producto_id;
        $productos->imagenes = date('Ymd');
        $productos->url = Str::slug($request->nombre, '-');

        $productos->save();

        $ci = $request->file('img');

        $this->validate($request,[
            'clave' => 'required',
            'nombre' => 'required',
            'descripcion' => 'required',
            'precio' => 'required',
            'cat_tipo_producto_id' => 'required',
            'img.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);


        foreach($request->file('img') as $image)
            {
                $imagen = $image->getClientOriginalName();
                $formato = $image->getClientOriginalExtension();
                $image->move(public_path().'/uploads/', $imagen);
 
                // Guardamos el nombre de la imagen en la tabla 'img_productos'
                DB::table('img_productos')->insert(
				    [
				    	'nombre' => $imagen, 
				    	'formato' => $formato,
				    	'producto_id' => $productos->id
			
				    ]
				);
 
            } 

            return redirect()->route('admin.productos.index')->with('success','¡Producto guardado exitosamente!');
    }



    public function show($id)
    {
        $productos = ProductoModel::find($id);

        // dd($productos->imagenesProductos());

        return view('admin.productos.show', compact('productos'));

    }



    public function edit($id)
    {
        $productos = ProductoModel::find($id);
        $imagenes = ProductoModel::find($id)->imagenesProductos;
        $categorias = CatalogoTipoProductoModel::get();

        return view('admin.productos.edit', compact('productos','imagenes','categorias'));
        
    }

    public function update(ItemUpdateRequest $request, $id)
    {        
        $productos= ProductoModel::find($id);
        $productos->clave = $request->clave;
        $productos->nombre = $request->nombre;
        $productos->descripcion = $request->descripcion;
        $productos->precio = $request->precio;
        $productos->cat_tipo_producto_id = $request->cat_tipo_producto_id;
        
        $productos->save();
 
        $ci = $request->file('img');
 
        // Si la variable '$ci' no esta vacia, actualizamos el registro con las nuevas imágenes
        if(!empty($ci)){
 
            
            $this->validate($request, [
 
                'clave' => 'required',
                'nombre' => 'required',
                'descripcion' => 'required',
                'precio' => 'required',
                'cat_tipo_producto_id' => 'required',
                'img.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
 
            ]);        
 
            // Recibimos una o varias imágenes y las actualizamos 
            foreach($request->file('img') as $image)
                {
                    $imagen = $image->getClientOriginalName();
                    $formato = $image->getClientOriginalExtension();
                    $image->move(public_path().'/uploads/', $imagen);
 
                    // Actualizamos el nuevo nombre de la(s) imagen(es) en la tabla 'img_productos'
                    DB::table('img_productos')->insert(
                        [
                            'nombre' => $imagen, 
                            'formato' => $formato,
                            'producto_id' => $productos->id
                        ]
                    );
 
                } 
 
        }
 
        // Redireccionamos con mensaje  
        return redirect()->route('admin.productos.index')->with('success','¡Producto actualizado!');
        
    }



    public function destroy($id)
    {
        $productos = ProductoModel::find($id);
 
        // Selecciono las imágenes a eliminar 
        $imagen = DB::table('img_productos')->where('producto_id', '=', $id)->get();        
        $imgfrm = $imagen->implode('nombre', ',');  
        //dd($imgfrm);        
 
        // Creamos una lista con los nombres de las imágenes separadas por coma
        $imagenes = explode(",", $imgfrm);
        
        // Recorremos la lista de imágenes separadas por coma
        foreach($imagenes as $image){
            
            // Elimino la(s) imagen(es) de la carpeta 'uploads'
            $dirimgs = public_path().'/uploads/'.$image;
            
            // Verificamos si la(s) imagen(es) existe(n) y procedemos a eliminar  
            if(File::exists($dirimgs)) {
                File::delete($dirimgs);                
            }
 
        }    
 
        
        // Borramos el registro de la tabla 'productos'
        ProductoModel::destroy($id); 
 
        // Borramos las imágenes de la tabla 'img_productos' 
        $productos->imagenesProductos()->delete();
 
        // Redireccionamos con mensaje 
        return redirect()->route('admin.productos.index')->with('success','¡Producto eliminado!');
       
    }
 
    // Eliminar imagen de un Registro
    public function eliminarimagen(Request $request)
    {
    
        // Elimino la imagen de la carpeta 'uploads'
        $imagen = ProductoGaleriaModel::select('nombre')->where('id', '=', $request->imgId)->get();
       
        $imgfrm = $imagen->implode('nombre', ', ');
        
        Storage::delete($imgfrm);

        ProductoGaleriaModel::destroy($request->imgId);

        $return_json = [
            'success' => true,
            'msg' => 'Se eliminó la imagen con éxito'
        ];
         

        return $return_json;

        // Redireccionamos con mensaje 
        // return redirect()->route('admin.productos.edit',[$pid])->with('success','¡Producto eliminado!');
        
    }
 






    }


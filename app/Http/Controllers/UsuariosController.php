<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\UsuarioModel;
use App\User;
use Illuminate\Support\Facades\Auth;

class UsuariosController extends Controller
{

    private $modelo;

    public function __construct()
    {
        $this->modelo = new UsuarioModel();
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function clientes()
    {
        $data['data'] = User::all();
        return view('Admin.clientes',  $data);
    }

    public function edit($id)
    {
        $item = $this->modelo->showOne($id);
        //$this->clientes();
        return view('Templates.perfil',  compact('item'));
    }
    public function perfilEdit($id)
    {
        $item = $this->modelo->showPerfil($id);

        return view('Templates.editar_perfil',  compact('item'));
    }
    public function update(Request $request, $id)
    {
        $this->validar_usuario($request);
        $item = $this->modelo->guardar($request, $id);
        return redirect()->route('usuario.edit',  compact('item'))->with('success', 'Perfil Actualizado');
    }
    public function create()
    {
        $users['users'] = \App\User::all();
        return view('Templates.agregar_perfil', $users);
    } 
    public function crear(Request $request)
    {
        $this->validar_usuario($request);
        $data = $this->modelo->crear($request);
        if ($data) {
            if (Auth::user()->tipo_usuario == 1) {
                return redirect()->route('perfil')->with('success', 'Cliente Creado');
            }
            return redirect()->route('cliente.index')->with('success', 'Perfil Creado');
        }
        return view('Templates.agregar_perfil');
    }
    public function validar_usuario($request)
    {
        return  $this->validate($request, [
            'nombre' => 'required|max:60',
            'paterno'     => 'required|max:50',
            'materno'      => 'required|max:50',
            'telefono'    => 'required|max:45'
        ]);
    }
    public function borrarClienteDatos($id)
    {
        $this->modelo->borrar($id);
        if (Auth::user()->tipo_usuario == 1) {
            return redirect()->route('perfil')->with('success', 'Registro eliminado');
        }
        return redirect()->route('cliente.index')->with('success', 'Registro eliminado');
    }
    public function direcciones()
    {
        $datas = $this->modelo->direcciones();
        return view('templates.vista_direcciones', compact('datas'));
    }
    public function userDirec($request)
    {
        $datas = $this->modelo->userDireccion($request);
        return view('templates.vista_direcciones', compact('datas'));
    }
    public function deleteAdress(Request $request)
    {
        $validate = $this->modelo->deleteAdrees($request->id);
        //$datas = $this->modelo->direcciones();
        if ($validate == 0) {
            $return_json = [
                'success' => true,
                'msg' => 'Se elimino con exito'
            ];
        } else {
            $return_json = [
                'success' => true,
                'msg' => 'Error no se puedo eliminar'
            ];
        }
        return $return_json;
    }
    public function regresar($mensaje)
    {
        return redirect()->route('vista.direcciones', compact('datas'))->with('success', $mensaje);

    }
    //Jalar esto al modal para agregar direcciones
    public function viewdCrear()
    {
        return view('Cliente.create_adrees');
    }
    public function guardarDirec(Request $request)
    {
        $datas = $this->modelo->crearDorec($request);
        if ($datas) {
            return redirect()->route('vista.direcciones', compact('datas'))->with('success', 'Dirección registrada');
        } else {
            return redirect()->route('vista.direcciones', compact('datas'))->with('success', 'La dirección no se pudo crear');       
         }
    }

    public function adreesProfile($request)
    {
        $data = $this->modelo->buscaradrees($request);
        return view('Cliente.editar_adrees', compact('data'));
    }
    public function actualizarDirec(Request $request, $id)
    {
        $items = $this->modelo->updateDirecciones($request, $id);
        $datas = $this->modelo->direcciones();
        if ($items) {
            return redirect()->route('vista.direcciones', compact('datas'))->with('success', 'Dirección actualizada');
        }
        return redirect()->route('vista.direcciones', compact('datas'))->with('success', 'La dirección no se pudo actulizar');       
    }
}

<?php

return [
    
    'title' => ('Tienda Online'),
    'home' => ('Inicio'),
    'formul' => ('Completa el formulario'),
    'usus' => ('Usuarios'),
    'inicio' =>  ('Tienda'),
    'home' => ('Mi Carrito'),
    'logout' => ('Cerrar Sesion'),
    'pass' => ('Cambiar Contraseña'),
    'perfil' => ('Mi Perfil'),
    'admin' => ('Inicio'),
    'prod' => ('Productos'),
    'vent' => ('Ventas'),
    'clientes' => ('Usuarios Registrados'),
    'nohay' => ('No hay registro '),
    'correo' => ('Usuario'),
    'passdw' => ('Contraseña'),
    'passconf' => ('Confirmar Contraseña'),
    'registrar' => ('Registrar'),
    'addres' => ('Mis Direcciones'),
    'carr' =>('Mi carrito de compras'),
    'misdirec'=> ('Sin Direcciones')
];
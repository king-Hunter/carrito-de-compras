<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Auth;

Auth::routes();
/*
|--------------------------------------------------------------------------
| Rutas Generals
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => ['web', 'auth']], function () {
    Route::get('/home', 'HomeController@carrito')->name('home');
    Route::get('/home/{usuario}/perfil', 'UsuariosController@edit')->name('usuario.edit');
    Route::get('/home/{id}/edit', 'UsuariosController@perfilEdit')->name('perfil');
    Route::put('/home/edit/{empleado}', 'UsuariosController@update')->name('usuario.update');
    Route::post('/home/edit', 'UsuariosController@crear')->name('usuario.guardar');
    Route::get('/home/crear', 'UsuariosController@create')->name('usuario.crear');
    Route::delete('/home/borrar/{empleado}', 'UsuariosController@borrarClienteDatos')->name('usuario.borrar');
    Route::get('/home/direcciones', 'UsuariosController@direcciones')->name('vista.direcciones');
    Route::get('/home/user/direcciones/{id}', 'UsuariosController@userDirec')->name('user.direcciones');
    Route::resource('admin/compras', 'ComprasController');
    Route::get('admin/compras/info/{id}','CompraProductoController@show')->name('infoCompra');
    Route::get('compras/historial/{id}','ComprasController@verHistorial')->name('historialCompras');

});

/*
|--------------------------------------------------------------------------
| Rutas Carrito
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => ['web', 'auth']], function () {
    Route::post('/home', 'HomeController@agregarProducto')->name('agregar.producto');

});

/*
|--------------------------------------------------------------------------
| Rutas de User
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => ['web', 'user']], function () {
    Route::get('/', 'HomeController@index')->name('index');
    Route::post('/home/enviar', 'CarritoController@enviar')->name('home.enviar');
    Route::post('/home/direccion/delete', 'UsuariosController@deleteAdress')->name('adress.delete');
    Route::post('/home/carrito/delete', 'CarritoController@deleteProduc')->name('carrito.delete');
    Route::post('/home/carrito/update', 'CarritoController@updateProduc')->name('carrito.update');
    Route::get('/home/direcciones/crear', 'UsuariosController@viewdCrear')->name('viewd.crear');
    Route::post('home/direcciones/guardar', 'UsuariosController@guardarDirec')->name('guardar.direccion');
    Route::get('/home/direcciones/{id}/guardar', 'UsuariosController@adreesProfile')->name('edit.adrees');
    Route::post('/home/direcciones/actializar/{id}', 'UsuariosController@actualizarDirec')->name('actualizar.adrees');
    Route::post('carrito/cliente','aCarritoSessionController@carrito_cliente')->name('carrito.cliente');
});

/*
|--------------------------------------------------------------------------
| Rutas de administrador
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => ['web', 'admin']], function () {
    Route::get('/home/usuarios', 'UsuariosController@clientes')->name('usuario');
        // Rutas del CRUD de productos

/* Crear */
Route::get('admin/producto/crear', 'ProductoController@create')->name('admin.productos.create');
Route::put('admin/producto/store', 'ProductoController@store')->name('admin.productos.store');
 
/* Leer */
Route::get('admin/producto', 'ProductoController@index')->name('admin.productos.index');
 
/* Actualizar */
Route::get('admin/producto/edit/{id}', 'ProductoController@edit')->name('admin.productos.edit');
Route::put('admin/producto/update/{id}', 'ProductoController@update')->name('admin.productos.update');
 
/* Eliminar */
Route::delete('admin/producto/destroy/{id}', 'ProductoController@destroy')->name('admin.productos.destroy');

/* Ver */
Route::get('producto/{id}','ProductoController@show')->name('admin.productos.show');


/* Eliminar imagen de un registro */
Route::post('admin/producto/eliminarImagen', 'ProductoController@eliminarImagen')->name('admin.productos.eliminarImagen');
//Rutas Listar compras
//Route::get('/compras', 'ComprasController@index');
//Route::get('/compras/create', 'ComprasController@create');


});



// Paymente
Route::get('/paypal/pay', 'PaymentController@payWithPayPal');
Route::get('/paypal/status', 'PaymentController@payPalStatus');




/*
|--------------------------------------------------------------------------
|  Ruta validacion
|--------------------------------------------------------------------------
*/


    //Route::resource('empleado', 'EmpleadoController');

   // Route::get('empleados','EmpleadoController@index')->name('empleado.index');
    //Route::get('empleado/create', 'EmpleadoController@create')->name('empleado.create');
    //Route::post('empleado', 'EmpleadoController@store')->name('empleado.store');
    //Route::get('empleado/{empleado}/edit','EmpleadoController@edit')->name('empleado.edit');
    //Route::delete('empleado/{empleado}','EmpleadoController@destroy')->name('empleado.destroy');
    //Route::get('empleado/{empleado}','EmpleadoController@show')->name('empleado.show');
    //Route::put('empleado/{empleado}', 'EmpleadoController@update')->name('empleado.update');

Route::get('/estilos', function(){
    return view('estilos.estilos');
});


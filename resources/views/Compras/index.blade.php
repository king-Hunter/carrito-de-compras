@extends('layouts.menu')
@section('content')
    <div class="container">
        <div class="row">
            
            <div class="col-7"></div>
            <div class="col-5">
                
            </div>
            <div class="card">
                
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <div class="card-header bg-info text-white">
                <h1 class="display-5">Lista de Compras</h1>
            </div>
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            <section class="example mt-4">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover    ">
                        <thead>
                            <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Fecha de compra</th>
                            <th class="text-center">Monto</th>
                            <th class="text-center">Folio</th>
                            <th class="text-center">¿Quien compro?</th>
                            <th class="text-center">Dirección</th>
                            <th class="text-center">Estatus</th>
                            <th class="text-center">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($compras as $compra)
                                <tr>
                                    <td>{{ $loop->iteration}}</td>
                                    <th scope="row"> {{ $compra->fecha_compra }}</th>
                                    <td>$ {{ $compra->monto }}</td>
                                    <td>{{ $compra->folio}}</td> 
                                    <td>{{ $compra->nombre}} {{$compra->paterno}}</td>
                                    <td>{{ $compra->calle}} {{$compra->colonia}}</td>
                                    <td>{{ $compra->fecha }} {{ $compra->nombre_status }}</td>
                                    <td><a class="btn btn-primary btn-sm"
                                        href="{{ route('infoCompra', $compra->compra_id) }}">Productos</a>
                                      
                                 @if (Auth::user()->tipo_usuario)
                                    <button type="button" data-id="{{$compra->compra_id}}" data-fecha_compra="{{$compra->fecha_compra}}" 
                                    data-folio="{{$compra->folio}}" data-monto="{{$compra->monto}}" data-nombre="{{$compra->nombre}}"
                                    data-paterno="{{$compra->paterno}}" data-calle="{{$compra->calle}}" data-colonia="{{$compra->colonia}}"
                                    class="btn btn-success btn-sm ver_detalles" data-toggle="modal" data-target="#VerDatosModal">Detalles</button></td>
                                    @endif
                                </tr>
                            @endforeach   
                        </tbody>
                    </table>
                    </div>
            </section>
            </div>
                <!-- Modal para Visualizar detalles de Compra -->
                        <div class="modal fade" id="VerDatosModal" data-bs-backdrop="static" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true" data-backdrop="static">
                        <div class="modal-dialog">
                            <div class="modal-content">
                            <div class="modal-header">
                                <div class="mb-3">
                                  <h5 class="modal-title" id="staticBackdropLabel">Datos de su compra <span id="compra_nombre"></span> </h5>
                                </div> 
                             <!--  <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>-->
                                 <br>  <div class="mb-3">
                                        <label for="formGroupExampleInput" class="form-label" style="background:yellow">Folio: <span id="compra_folio"></span></label>
                                    </div>
                            </div>
                            <div class="modal-body">
                                    <div class="mb-3">
                                        <label for="formGroupExampleInput" class="form-label">Fecha de compra: <span id="compra_fecha"></span></label>
                                    </div>
                                    <div class="mb-3">
                                        <label for="formGroupExampleInput" class="form-label">Monto: $<span id="compra_monto"></span></label>
                                    </div>
                                    <div class="mb-3">
                                        <label for="formGroupExampleInput" class="form-label">Direccion: <span id="compra_calle"></span></label>
                                        <label for="formGroupExampleInput" class="form-label">Colonia: <span id="compra_colonia"></span></label>
                                    </div>
                                    <br>
                                    {!! Form::model($compra, ['id'=>'actualizar','method'=>'PATCH','route'=>['compras.update',1]]) !!}
                                        {{ csrf_field() }}
                                        <input type="text" name="compra_id" value="" id="compra_id" hidden="visibility">
                                        <input type="text" name="fecha" value="{{ now() }}" id="fecha" hidden="visibility">
                                        <label>Selecciona para cambiar estatus</label> <br>
                                        <select name="nombre_status" id="nombre_status" class="form-select" aria-label="Default select example">
                                            <!--  @foreach($compras as $c)
                                                <option value="{{ $c->id }}">{{ $c->nombre_status }}</option>
                                            @endforeach-->
                                            <option>Por pagar</option>
                                            <option>Pagado</option>
                                            <option>Entregado</option>
                                            <option>Pago Rechazado</option>
                                            <option>Enviado</option>
                                        </select> <br>

                                      <br>  <button type="submit" class="btn btn-primary">Guardar</button>
                                    {!! Form::close() !!}
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                
                            </div>
                            </div>
                        </div>
                        </div>
                        <!--Fin de Modal para ver detalles-->
        </div>
    </div>
@endsection
@section('js')
    
    <script type="text/javascript">

        $(document).ready(function(){
           // alert('Entra a la funcion');
          // let id_compras;
            $(document).on('click', '.ver_detalles', function(){

                let id_compras = $(this).attr('data-id');
                let fecha_compra = $(this).data('fecha_compra');
                let folio = $(this).data('folio');
                let monto = $(this).data('monto');
                let nombre = $(this).data('nombre');
                let paterno = $(this).data('paterno');
                let calle = $(this).data('calle');
                let colonia = $(this).data('colonia');
    
                console.log('prueba');
                console.log(id_compras);
                    $('#compra_id').html(id_compras);
                    $('#compra_fecha').html(fecha_compra);
                    $('#compra_folio').html(folio);
                    $('#compra_monto').html(monto);
                    $('#compra_nombre').html(nombre);
                   // $('#compra_nombre').append(paterno);
                    $('#compra_calle').html(calle);
                    $('#compra_colonia').html(colonia);;

                    $('#compra_id').val(id_compras);
                    // Ruta para actualizar
                    let ruta = '{{ url('admin/compras') }}/'+id_compras;
                    $('#actualizar').attr('action',ruta);

                    
            });
        })
        
    </script>
@endsection
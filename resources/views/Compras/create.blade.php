@extends('layouts.menu')
@section('content')
     <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                 <form action="">
                 {{ csrf_field()}}
                        <label for="fecha_compra">Fecha de compra</label>
                        <input class="form-control" type="datetime" name="fecha_compra">
                        
                        <a href="#" class="btn btn-success" type="submit" value="agregar">Agregar</a>
                 </form>    
                </div>
            </div>
        </div>
    </div>
@endsection
@extends('layouts.menu')
@section('content')
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width:100%;">
            <div class="card-header bg-info text-white">
            </div> 
            <div class="card-body">
                <section>
                  <!--  <span>{{ $compra_producto }}</span>-->
                    @foreach($compra_producto as $com)
                    <h6><strong>Precio de tu compra:</strong></h6>
                    ${{ $com->precio_compra}}.00
                    <br>
                        @foreach($com->productos->imagenesProductos as $img)
                        <a data-fancybox="gallery" href="{{url('/')}}/uploads/{{ $img->nombre }}">
                            <img src="{{url('/')}}/uploads/{{ $img->nombre }}" width="200" class="img-fluid">
                        </a>
                        @endforeach
                        <h6><strong>Cantidad:</strong></h6>
                        {{ $com->cantidad}}
                        <br>
                        <hr>
                    @endforeach
                    
                </section>
            </div>
            @if (Auth::user()->tipo_usuario)
            <!-- Admin -->
            <div class="card-footer text-center">
                <a href="{{ route('compras.index') }}" class="btn btn-info btn-lg  ml-3">Volver</a>
            </div>
            @else
            <!-- Usuario -->
            <div class="card-footer text-center">
                <a href="{{ route('historialCompras', Auth::user()->id) }}" class="btn btn-info btn-lg  ml-3">Volver</a>
            </div>
            @endif
        </div>
    </div>
@endsection
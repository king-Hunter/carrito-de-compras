@extends('layouts.menu')
@section('css')
    <style>
        .mybutoom {
            padding-left: 4rem;
            padding-right: 4rem;
            font: -webkit-small-control;
        }
    </style>
@endsection
@section('content')
<div class="row justify-content-center align-items-center py-5">
    <form method="POST" action="{{ route('guardar.direccion') }}"  role="form">
        {{ csrf_field() }}
        <div class="form-group">
          <label for="calle">Calle</label>
          <input type="text" class="form-control" id="calle" name="calle" placeholder="1234 Main St" required>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="colonia">Colonia</label>
                <input type="text" class="form-control" id="colonia" name="colonia" required>
              </div>
              <div class="form-group col-md-6">
                <label for="municipio">Municipio</label>
                <input type="text" class="form-control" id="municipio" name="municipio" required>
              </div>
              <div class="form-group col-md-6">
                <label for="estado">Estado</label>
                <input type="text" class="form-control" id="estado" name="estado" required>
              </div>
              <div class="form-group col-md-6">
                <label for="referencia">Referencia</label>
                <input type="text" class="form-control" id="referencia" name="referencia" required>
              </div>
        </div>
        <div class="form-row ">
            <div class="form-group col-md-4">
                <label for="codigo_postal">Codigo Postal</label>
                <input type="text" class="form-control" id="codigo_postal" name="codigo_postal" required>
              </div>
          <div class="form-group col-md-4">
            <label for="numero_ext">Numero Interior</label>
            <input type="text" class="form-control" id="numero_int" name="numero_int" required>
          </div>
          <div class="form-group col-md-4">
            <label for="numero_ext">Numero Exterior</label>
            <input type="text" class="form-control" id="numero_ext" name="numero_ext" required>
          </div>
        </div>
        <button type="submit" class="btn-block btn btn-primary">Guardar</button>
      </form>
</div>
@endsection

@extends('layouts.menu')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Mi Carrito de compras</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        

                        <div class="list-group">
                            <a href="#" class="list-group-item">
                                <buttom href="#" class=" btn btn-danger"> x </buttom> Cras justo
                                odio
                            </a>
                            
                        </div>
                        <div class="col-xs-12 col-md-12">
                            <a href="#" class="btn btn-success">Pagar</a>
                            <a href="#" class="btn btn-danger">Cancelar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

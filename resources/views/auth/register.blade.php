@extends('layouts.menu')
@section('css')
    <style>
        .regis {
            width: 9rem;
            margin-left: 6rem;
            margin-right: 6rem;
        }

    </style>
@endsection
@section('content')
    <div class="row justify-content-center align-items-center py-5">
        <h2>{{ config('textos.registrar') }}se</h2>
        <div class="col-12">
            <div class="container-fluid">
                <div class="row justify-content-center align-items-center py-5">
                    <form method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">{{ config('textos.correo') }}</label>
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                                required>
                            @if ($errors->has('email'))
                                <div class="position-fixed bottom-0 right-0 p-3" style="z-index: 5; right: 0; top: 0;">
                                    <div id="liveToast" class="toast hide" role="alert" aria-live="assertive"
                                        aria-atomic="true" data-delay="5000">
                                        <div class="toast-header">
                                            <strong class="mr-auto">Alerta Usuario</strong>
                                            <small>{{ now() }}</small>
                                            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast"
                                                aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="toast-body text-center">
                                            {{ $errors->first('email') }}
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password">{{ config('textos.passdw') }}</label>
                            <input id="password" type="password" class="form-control" name="password" required>
                            @if ($errors->has('password'))
                                <div class="position-fixed bottom-0 right-0 p-3" style="z-index: 5; right: 0; top: 0;">
                                    <div id="liveToast" class="toast hide" role="alert" aria-live="assertive"
                                        aria-atomic="true" data-delay="5000">
                                        <div class="toast-header">
                                            <strong class="mr-auto">Alerta Contraseña</strong>
                                            <small>{{ now() }}</small>
                                            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast"
                                                aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="toast-body text-center">
                                            {{ $errors->first('password') }}
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password-confirm">{{ config('textos.passconf') }}</label>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                                required>
                        </div>
                        <button type="bottom" class="regis btn btn-primary">{{ config('textos.registrar') }}se</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            $(".toast").toast('show');
        });

    </script>
@endsection

@extends('layouts.menu')
@section('css')
    <style>
        .card-title {
            color: #000000 !important;
            text-decoration: none !important;
        }

        .deco {
            text-decoration: none !important;
        }

        .py-5 {
            padding-left: 2rem;
            padding-right: 2rem;
        }

    </style>
@endsection
@section('content')
    <div class="row h-100 justify-content-center align-items-center py-5">
        <a class="deco py-5" href="{{ route('admin.productos.index') }}">
            <div class="card" style="width: 18rem;">
                <img src="{{ asset('img/prd.jpg') }}" class="card-img-top" alt="productos">
                <div class="card-body">
                    <h5 class="card-title">Productos</h5>
                </div>
            </div>
        </a>
        <a class="deco py-5" href="{{ route('usuario') }}">
            <div class="card" style="width: 18rem;">
                <img src="{{ asset('img/clientes.png') }}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Usuarios</h5>
                </div>
            </div>
        </a>
        <a class="deco py-5" href="{{ action('ComprasController@index') }}">
            <div class="card" style="width: 18rem;">
                <img src="{{ asset('img/ventas.jpg') }}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Ventas</h5>
                </div>
            </div>
        </a>

    </div>


@endsection
@section('js')
    <script>


    </script>
@endsection

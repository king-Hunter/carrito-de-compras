@extends('layouts.menu')
@section('css')
    <style>
        a {
            text-decoration: none !important;
        }

        hr {
            border-top: 1px solid white;
        }

        .mybutoom {
            padding-left: 4rem;
            padding-right: 4rem;
            font: -webkit-small-control;
        }
        .addeli{
            margin-left: 1rem;
            margin-right: 1rem;
        }
        .colo{
            color: gray !important;
            font-weight: 900;
        }
        #mensajeEliminar{
            position: fixed;
        right: 1rem;
        }
        .toast {
    opacity: 1;
 
}
    </style>
@endsection
@section('content')
<div class="alert alert-primary" role="alert" id="mensajeEliminar" style="display:none">
    <div class="position-fixed bottom-0 right-0 p-3" style="z-index: 5; right: 0; top: 0; background: transparent">
        <div id="liveToast" class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-delay="5000">
            <div class="toast-header">
                <small>{{ now() }}</small>
            </div>
            <div class="toast-body text-center">
                <strong>Se eliminó correctamente la dirección</strong>
            </div>
        </div>
    </div>
  </div>

    <div class="jumbotron p-4 p-md-5 text-white rounded bg-dark">
        <div class="col-md-8 px-0">
            @if (!Auth::user()->tipo_usuario)
            <h1 class="display-4 font-italic">Mis Direcciones</h1>
            <a type="bottom" class="mybutoom btn btn-lg btn-info" href="{{ route('viewd.crear') }}">Agregar</a>
            @else
            <h1 class="display-4 font-italic">Direcciones</h1>
            @endif
            <hr>
            @if ($datas->cantidad == 0)
                <h1><a class="text-white font-weight-bold">
                        {{ config('textos.misdirec') }}
                    </a></h1>
            @else
                @foreach ($datas as $data)
                <div class="direccion{{ $data->id }}">

                @if (!$data->status)
                <span class="badge badge-danger">Direccion eliminada por el usuario 
                    y guardada por el registro de una o varias compras</span>
                    @endif
                    <div class="row " id="direc">
                        <div class="col-6">
                            <p class="lead mb-0"><a class="text-white font-weight-bold">Calle</a></p>
                            <p class="lead my-3">{{ $data->calle }}</p>
                        </div>
                        <div class="col-6">
                            <p class="lead mb-0"><a class="text-white font-weight-bold">Estado</a></p>
                            <p class="lead my-3">{{ $data->estado }}</p>
                        </div>
                        <div class="col-6">
                            <p class="lead mb-0"><a class="text-white font-weight-bold">Municipio</a></p>
                            <p class="lead my-3">{{ $data->municipio }}</p>
                        </div>
                        <div class="col-6">
                            <p class="lead mb-0"><a class="text-white font-weight-bold">Colonia</a></p>
                            <p class="lead my-3">{{ $data->colonia }}</p>
                        </div>
                        <div class="col-6">
                            <p class="lead mb-0"><a class="text-white font-weight-bold">Numero Exterior</a></p>
                            <p class="lead my-3">{{ $data->numero_ext }}</p>
                        </div>
                        <div class="col-6">
                            <p class="lead mb-0"><a class="text-white font-weight-bold">Numero Interior</a></p>
                            <p class="lead my-3">{{ $data->numero_int }}</p>
                        </div>
                        <div class="col-6">
                            <p class="lead mb-0"><a class="text-white font-weight-bold">Referencia</a></p>
                            <p class="lead my-3">{{ $data->referencia }}</p>
                        </div>
                        <div class="col-6">
                            <p class="lead mb-0"><a class="text-white font-weight-bold">Codigo Postal</a></p>
                            <p class="lead my-3">{{ $data->codigo_postal }}</p>
                        </div>
                    </div>
                    @if (!Auth::user()->tipo_usuario)
                    <a type="bottom" class="mybutoom addeli btn btn-lg btn-warning" href="{{ action('UsuariosController@adreesProfile', $data->id) }}">Modificar</a>
                    <a type="bottom" onclick="confirmarEliminar({{ $data->id }})" class="mybutoom btn btn-lg btn-danger" >Eliminar</a>
                    @endif
                    <hr>
                </div>
                @endforeach
            @endif
        </div>
       
    </div>
 @if (Session::has('success'))
        <div class="position-fixed bottom-0 right-0 p-3" style="z-index: 5; right: 0; top: 0; background: transparent">
            <div id="liveToast" class="toast tod" role="alert" aria-live="assertive" aria-atomic="true" data-delay="5000">
                <div class="toast-header">
                    <small>{{ now() }}</small>
                </div>
                <div class="toast-body text-center">
                    <strong> {{ Session::get('success') }}</strong>
                </div>
            </div>
        </div>
    @endif
@endsection
@section('js')

<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
<script type="text/javascript">

$(".tod").delay(3200).fadeOut(300);

  function confirmarEliminar(id) {
    var route = "{{route("adress.delete")}}";
    var x = confirm("Eliminar esta dirección ?");
    if (x)
      // return true;
      $.ajax({
        type: 'POST', //tipo de peticion
        url: route, //direccion de la peticion url
        data: {
          id: id,
          _token: '{{csrf_token()}}',
        }, //paramentros a enviar por la peticion
        dataType: 'json',
        success: function (respuestaAjax) {
            $("#mensajeEliminar").show();
          $(".direccion"+ id).remove();
          setTimeout(() => {
            $("#mensajeEliminar").hide();
          }, 2000);
        },
        error: function (err) {
          alert('hubo un error en la petición');
          alert(err.status);
        }
      });

    else
      return false;
  }
</script>
@endsection

@extends('layouts.menu')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading ">
                        <label>Editar Usuario</label>
                    </div>
                    <br>
                    <div class="panel-body">
                        <form method="POST" action="{{ route('usuario.guardar') }}"  role="form">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label>Nombre </label>
                                        <input type="text" name="nombre" id="nombre" class="form-control input-sm"  placeholder="sadahi">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label>Apellido Paterno</label>
                                        <input type="text" name="paterno" id="paterno" class="form-control input-sm" placeholder="rogelio">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label>Apellido Materno</label>
                                        <input type="text" name="materno" id="materno" class="form-control input-sm" placeholder="florin">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label>Telefono</label>
                                        <input type="number" name="telefono" id="telefono" class="form-control input-sm" placeholder="33423545">
                                    </div>
                                </div>
                                @if (Auth::user()->tipo_usuario==1)
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label>Sincronizar usuario</label>
                                            <select class="form-control" name="users_id" id="users_id" placeholder="Tipo de Usuario">
                                            @foreach ($users as $options)
                                            <option value="{{ $options->id }}" >{{ $options->email }}</option>
                                                @endforeach
                                              </select>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <input type="submit"  value="Actualizar" class="btn btn-success btn-block">
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <a href="{{ route('perfil') }}" class="btn btn-info btn-block" >Atrás</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

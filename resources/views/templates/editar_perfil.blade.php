@extends('layouts.menu')
@section('content')
<div class="row justify-content-center align-items-center py-5">
    <h2>Editar Usuario</h2>
    <div class="col-12">
        <div class="row justify-content-center align-items-center ">
            <small id="emailHelp" class="form-text text-muted"></small>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row justify-content-center align-items-center py-5">
            <div class="col-10">
                <form method="POST" action="{{ route('usuario.update',$item->id) }}  "  role="form">
                    {{ method_field('PUT') }}
                            {{ csrf_field() }}
                    <div class="form-row">
                      <div class="form-group col-md-6">
                        <label for="nombre">Nombre</label>
                        <input type="text" name="nombre" id="nombre" class="form-control " value="{{ $item->nombre }}">                        
                      </div>
                      <div class="form-group col-md-6">
                        <label for="paterno">Apellido Paterno</label>
                        <input type="text" name="paterno" id="paterno" class="form-control " value="{{ $item->paterno }}">
                      </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="materno">Apellido Materno</label>
                          <input type="text" name="materno" id="materno" class="form-control " value="{{ $item->materno }}">
                        </div>
                        <div class="form-group col-md-6">
                          <label for="telefono">Telefono</label>
                          <input type="tel" name="telefono" id="telefono" class="form-control" value="{{ $item->telefono }}">
                        </div>
                      </div>
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <input type="submit"  value="Guardar" class="btn btn-success btn-block">
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <a href="{{action('UsuariosController@edit', Auth::user()->id)}}" class="btn btn-info btn-block" >Atrás</a>
                        </div>
                    </div>
                  </form>
            </div>        
        </div>
    </div>
</div>
@endsection
@section('js')
<script type='text/javascript' src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
    <script >
        $(window).on('load',function()
{
   var phones = [{ "mask": "(###) ###-####"}, { "mask": "(###) ###-##############"}];
    $('#telefono').inputmask({ 
        mask: phones, 
        greedy: false, 
        definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
});
    </script>
@endsection
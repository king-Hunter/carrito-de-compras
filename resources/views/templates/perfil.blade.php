@extends('layouts.menu')
@section('css')
    <style>
        a {
            text-decoration: none !important;
        }

        .mybutoom {
            padding-left: 4rem;
            padding-right: 4rem;
            font: -webkit-small-control;
        }

    </style>
@endsection
@section('content')
    <div class="jumbotron p-4 p-md-5 text-white rounded bg-dark">
        <div class="col-md-8 px-0">
            <h1 class="display-4 font-italic">{{ $item->full_name }}</h1>
            <p class="lead mb-0"><a class="text-white font-weight-bold">
                    @if ($item->tipo_usuario == 1)
                        Administrador
                    @else
                        Cliente
                    @endif
                    #{{ $item->users_id }} :
                </a></p>
            <p class="lead my-3">{{ $item->email }}</p>
            <p class="lead mb-0"><a class="text-white font-weight-bold">Telefono</a></p>
            <p class="lead my-3">{{ $item->telefono }}</p>
            @if ($item->dataCampos == '' || $item->dataCampos == null)
                
            @if (!Auth::user()->tipo_usuario)
            <p class="lead mb-0"><a class="text-white font-weight-bold">Mis Direcciones</a></p>
            <p class="lead my-3"> </p>
            @else
            <p class="lead mb-0"><a class="text-white font-weight-bold">Direcciones</a></p>
            <p class="lead my-3"> </p>
            @endif
                @if ($item->direccion > 0)
                            Tiene {{ $item->direccion }} 
                            @if ($item->direccion == 1)
                                Dirección 
                            @else
                                Direcciones 
                                @endif              
                                
                                @if (!Auth::user()->tipo_usuario)
                                <a type="buttom" href="{{ route('vista.direcciones') }}" class="btn btn-danger mybutoom">        
                                @else
                                <a type="buttom" href="{{ route('user.direcciones', $item->users_id) }}" class="btn btn-danger mybutoom">
                                @endif
                                    @if ($item->direccion == 1)
                                    Ver
                                    @else
                                        Ver lista
                                    @endif
                                </a>
                    </p>
                
                            @else
                                @if (!Auth::user()->tipo_usuario)
                                    <p class="lead my-3"> No tienes direcciones registradas <a type="buttom"
                                        href="{{ route('vista.direcciones') }}" class="btn btn-danger mybutoom">Agregar Nueva</a> </p>
                                @else
                                <p class="lead my-3">
                                    Este Usuario aun no tiene direcciones
                                </p>
                    @endif

            @endif
            <p class="lead mb-0"><a class="text-white font-weight-bold">Historial de compras</a></p>
            <!--<p class="lead my-3">No tiene compras realizadas </p>-->
            <a href="{{ route('historialCompras', Auth::user()->id) }}" class="btn btn-primary">Ver compras</a>
            @endif

            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <label> </label>
                        <br>
                        @if (Auth::user()->tipo_usuario == 1)
                            <label>
                                <h4>{{ $item->dataCampos }}</h4>
                            </label>
                        @else
                            <a href="{{ action('UsuariosController@perfilEdit', $item->id) }}"
                                class="btn btn-warning btn-block">Cambiar</a>
                        @endif
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <label> </label>
                        <br>
                        @if (Auth::user()->tipo_usuario == 1)
                            <a href="{{ route('usuario') }}" class="btn btn-info btn-block">Atrás</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if (Session::has('success'))
        <div class="position-fixed bottom-0 right-0 p-3" style="z-index: 5; right: 0; top: 0; background: transparent">
            <div id="liveToast" class="toast hide" role="alert" aria-live="assertive" aria-atomic="true" data-delay="5000">
                <div class="toast-header">
                    <strong class="mr-auto">Exito</strong>
                    <small>{{ now() }}</small>
                    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="toast-body text-center">
                    {{ Session::get('success') }}
                </div>
            </div>
        </div>
    @endif

@endsection
@section('js')
    <script>
        $(document).ready(function() {
            $(".toast").toast('show');
        });

    </script>
@endsection

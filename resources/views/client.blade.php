@extends('layouts.menu')
@section('css')
    <style>
        .btn-sm {
            font-weight: 900;
            font-size: 10px !important;
        }

        .espacios {
            position: absolute;
        }

        .botonesmeno {
            position: relative;
            left: 1rem;
        }

        .botonesmas {
            position: relative;
            right: 1rem;
        }

        .compra {
            position: fixed;
            bottom: 3rem;
            right: 1rem;
            padding-left: 2rem;
            padding-right: 2rem;
            z-index: 1;
        }
        
        #mensajeEliminar{
            position: fixed;
        right: 1rem;
        }
        .unPrecio{
          z-index: -1;
        }
    </style>
@endsection
@section('content')

@if (session('status'))
    <div class="alert alert-danger" role="alert">
        <h3>{{session('status')}}</h3>
    </div>
         
    @endif


<div class="alert alert-primary" role="alert" id="mensajeEliminar" style="display:none">
    "Se eliminó correctamente del carrito"
  </div>

    <div class="row justify-content-center align-items-center py-5">
        <h2>{{ config('textos.carr') }}</h2>
    </div>
    <div class="row justify-content-center align-items-center py-2">
        @if ($data->count())

            <button type="button" class="btn btn-info compra" data-userid="{{ $data[0]->users_id }}" data-productoid="{{ $data[0]->id }}" data-toggle="modal" data-target="#staticBackdrop">Comprar</button>

            <table class="table table-borderless table-hover">
                <thead class="bg-info text-white">
                    <tr>
                        <th scope="col" class="text-center">Cantidad</th>
                        <th scope="col" class="text-center">Producto</th>
                        <th scope="col" class="text-center">Precio</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $item)
                        <tr class="producto{{ $item->id }}">
                            <th scope="row">
                                <div class="row justify-content-center align-items-center">
                                    <a type="buttom" onclick="cambiarCantidad({{ $item->id }},{{ $item->cantidad }}, false)" class="btn btn-sm btn-danger botonesmas"> - </a>
                                    <span id="canti" class="espacios"> {{ $item->cantidad }} </span>
                                    <a type="buttom" onclick="cambiarCantidad({{ $item->id }},{{ $item->cantidad }}, true)" class="btn btn-sm btn-primary botonesmeno">+</a>
                                </div>
                            </th>
                            <td><a class="list-group-item list-group-item-action"> {{ $item->nombre }} </a>
                            </td>
                            <td><a class="list-group-item list-group-item-action">Total $<span class="preci">{{ $item->precio }} </span></a>
                            </td>
                            <td><a onclick="confirmarEliminar({{ $item->id }})" type="buttom" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="row justify-content-center align-items-center recd">
              <a class="list-group-item list-group-item-action"> Total $ <span class="unPrecio"></span> </a>
            </div>
        @else
            <a class="list-group-item list-group-item-action">
                {{ config('textos.nohay') }} de compras
            </a>
        @endif
    </div>
    
<!--Inicia Modal para trabajar-->
    <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Mi Compra <span class=" fa fa-shopping-cart"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="GET"  action="{{url('/paypal/pay')}}"  role="form">
                    {{ csrf_field() }}
                <div class="modal-body">
                  <div class="form-group">
                    <p><strong>Fecha de Transacción:</strong> <span id="fecha">{{ now() }}</span></p>
                  </div>
                  <div class="mb-3">
                    <label for="formGroupExampleInput" class="form-label">Nombre:</label>
                    <span id="nombre"></span>
                </div>
                        <div class="mb-3">
                            <label for="formGroupExampleInput" class="form-label">Monto: $<span id="compra_monto"></span></label>
                            <input type="text" name="input_compra_monto" id="input_compra_monto" hidden="visibility">
                        </div>
                        <div class="form-group">
                             
                          <label for="exampleFormControlSelect1"><strong>Seleccione una Dirección:</strong></label>
                            <select class="form-control" name="direccion_usuario_id" id="direccion_usuario_id">
                              @foreach($direcciones as $direccion)
                                    <option value="{{ $direccion->id }}">{{ $direccion->calle }} {{ $direccion->colonia }} {{ $direccion->municipio }} {{ $direccion->estado }}</option> 
                              @endforeach
                            </select>
                        </div>
                        <a href="{{ route('viewd.crear') }}" class="btn btn-primary">Agregar direccion</a>
                </div>
                <div class="modal-footer">
                  
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary"> Pagar con Paypal</button>
                </div>
            </form>
            </div>
        </div>
    </div>
    <!--Finaliza Modal para trabajar-->
@endsection
@section('js')
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
  var ejme=0;
  $( document ).ready(function() {
    
    $.each(<?php echo $data ?>,function(index,producto_carrito){
            var total = producto_carrito.cantidad * parseFloat(producto_carrito.precio);
            ejme = ejme + (producto_carrito.cantidad * parseFloat(producto_carrito.precio));
            var tr_producto = $('tr.producto'+producto_carrito.id);
            tr_producto.find('span.espacios').html(producto_carrito.cantidad);
            tr_producto.find('span.preci').html(total);
            var functioncambiarPr = 'cambiarCantidad('+producto_carrito.id+','+producto_carrito.cantidad+',true)';
            var functioncambiarPrmas = 'cambiarCantidad('+producto_carrito.id+','+producto_carrito.cantidad+',false)';
            tr_producto.find('a.botonesmeno').attr('onclick',functioncambiarPr);
            tr_producto.find('a.botonesmas').attr('onclick',functioncambiarPrmas);
            
          });
          $('.unPrecio').html(ejme);
          $('#compra_monto').html(ejme);

});

  function cambiarCantidad(id,canti,val) {
    var route = "{{route("carrito.update")}}";
      let cantidad;
      if (val) {
        this.cantidad = canti+1;
      } else {
        if (canti<1){
          this.cantidad = 1;
        } else{
          this.cantidad = canti-1;
        }
      }
      $.ajax({
        type: 'POST', //tipo de peticion
        url: route, //direccion de la peticion url
        data: {
          id: id,
          cantidad: this.cantidad,
          _token: '{{csrf_token()}}',
        }, //paramentros a enviar por la peticion
        dataType: 'json',
        success: function (respuestaAjax) {
          //console.log('{{ $data }}');
          //console.log(respuestaAjax.data);
          //var Totaltotal;
          var ejme=0;
          $.each(respuestaAjax.data,function(index,producto_carrito){
            var total = producto_carrito.cantidad * parseFloat(producto_carrito.precio);
            ejme = ejme + (producto_carrito.cantidad * parseFloat(producto_carrito.precio));
            //Totaltotal = Totaltotal+total;
            //obtener el row del producto cesta
            var tr_producto = $('tr.producto'+producto_carrito.id);
            tr_producto.find('span.espacios').html(producto_carrito.cantidad);
            tr_producto.find('span.preci').html(total);
            var functioncambiarPr = 'cambiarCantidad('+producto_carrito.id+','+producto_carrito.cantidad+',true)';
            var functioncambiarPrmas = 'cambiarCantidad('+producto_carrito.id+','+producto_carrito.cantidad+',false)';
                //console.log(functioncambiarPr);
            tr_producto.find('a.botonesmeno').attr('onclick',functioncambiarPr);
            tr_producto.find('a.botonesmas').attr('onclick',functioncambiarPrmas);

          });
          $('.unPrecio').html(ejme);
          $('#compra_monto').html(ejme);
          //var totals = $('a.unPrecio');
          //totals.find('a.unPrecio').html(222);
            //$("#mensajeEliminar").show();
          //$(".producto"+ id).remove();
          setTimeout(() => {
            //$("#mensajeEliminar").hide();
          }, 2000);
        },
        error: function (err) {
          alert('hubo un error en la petición');
          alert(err.status);
        }
      });
  }

      $('.compra').on('click', function(){
        document.getElementById("compra_monto");
        let id_compras = $(this).attr('data-userid');
         
          $('#compra_monto').html(ejme);
          $('#input_compra_monto').val(ejme);

          $('#compra_id').val(id_compras);
    
          let user_id= $(this).attr('data-userid');
          let producto_id= $(this).attr('data-productoid');
         
          $.ajax({
              type: 'POST',
              url:  "{{ route('carrito.cliente') }}",
              data: {
                "user_id": user_id,
                "producto_id": producto_id,
                "compra_monto": ejme,
                _token: '{{csrf_token()}}',
              },
              dataType: 'JSON',
              success: function(data){
                console.log(data);
                  $("#nombre").html(data['full_name']);
                  $("#producto").html(data['cantidad']);
              },
              error: function (err){
                console.log(err);
              }
          });
          


      });
      function confirmarEliminar(id) {
            var tr_producto = $('div.recd');
            var tr_produc = $('tr.producto'+id);
            var tryr = tr_produc.find('span.preci').html();
            var troe = tr_producto.find('span.unPrecio').html();
            var  ejs = troe - tryr;
    var route = "{{route("carrito.delete")}}";
    var x = confirm("Eliminar este producto de tu carrito ?");
    if (x)
      // return true;
      $.ajax({
        type: 'POST', //tipo de peticion
        url: route, //direccion de la peticion url
        data: {
          id: id,
          _token: '{{csrf_token()}}',
        }, //paramentros a enviar por la peticion
        dataType: 'json',
        success: function (respuestaAjax) {
          $(".producto"+ id).remove();

            //ejme = ejme - (producto_carrito.cantidad * parseFloat(producto_carrito.precio));
            console.log(ejs);
          $('.unPrecio').html(ejs);
          $('#compra_monto').html(ejs);

            $("#mensajeEliminar").show();
          $(".producto"+ id).remove();

          setTimeout(() => {
            $("#mensajeEliminar").hide();
          }, 2000);
        },
        error: function (err) {
          alert('hubo un error en la petición');
          alert(err.status);
        }
      });

    else
      return false;
  }
</script>
@endsection

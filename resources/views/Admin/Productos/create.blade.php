@extends('layouts.menu')
@section('content')

<div class="container">
    <div class="row">
        <div class="col align-self-end">
            <div class="card">
                <div class="card-header bg-info text-white">
                    <h4 class="display-5">Nuevo producto</h4>
                </div>
                <div class="card-body">
                    <section>
                        <form method="POST" action="{{ route('admin.productos.store') }}" role="form"
                            enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div>
                                @include('admin.productos.frm.form')
                            </div>
                        </form>
                    </section>
                </div>
                <div class="card-footer text-center">
                    <a href="{{ route('admin.productos.index') }}" class="btn btn-success ml-3">Volver</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@extends('layouts.menu')
@section('content')

<div class="container-fluid p-4">
    <div class="row">
        <div class="card">
            <div class="card-header bg-info text-white">
                <h1 class="display-5">Productos</h1>
            </div>
            <div class="panel-body">
                <div id="mensajeExito" class="alert alert-success" role="alert">
                    @if(Session::has('success'))
                    {{ Session::get('success') }}
                    @endif
                </div>
                <div class="col-sm-4">
                    <a href="{{ route('admin.productos.create') }}" class="btn btn-info btn-lg mt-4 ml-3"> Agregar</a>
                </div>
                <section class="example mt-4">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width:250px;">Acciones</th>
                                    <th class="text-center">Clave</th>
                                    <th class="text-center">Nombre</th>
                                    <th class="text-center" style="width:600px;">Descripción</th>
                                    <th class="text-center">Precio</th>
                                    <th class="text-center">Tipo producto</th>
                                    <th class="text-center">Imagen</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($productos as $prod)
                                <tr>
                                    <td class="v-align-middle text-center">
                                        <form class="formulario{{$prod->id}}"
                                            action="{{action('ProductoController@destroy', $prod->id)}}" method="post">
                                            {{csrf_field()}}
                                            <input name="_method" type="hidden" value="DELETE">
                                            <a class="btn btn-outline-primary "
                                                href="{{action('ProductoController@show', $prod->id)}}">Ver</a>
                                            <a class="btn btn-outline-secondary "
                                                href="{{action('ProductoController@edit', $prod->id)}}">Editar</a>

                                            <button data-producto_id="{{$prod->id}}"
                                                class="btn btn-outline-danger btn-eliminar-prod"
                                                type="button">Eliminar</button>
                                        </form>
                                    </td>
                                    <td class="v-align-middle">{{$prod->clave}}</td>
                                    <td class="v-align-middle">{{$prod->nombre}}</td>
                                    <td class="v-align-middle">{{$prod->descripcion}}</td>
                                    <td class="v-align-middle">{{$prod->precio}}</td>
                                    <td class="v-align-middle">{{$prod->categoriaProducto->nombre}}</td>
                                    <td class="v-align-middle text-center">
                                        <!-- @foreach($prod->imagenesProductos as $img)
                                    <img src="../uploads/{{$img->nombre}}" width="30" class="img-responsive">
                                    @endforeach -->
                                        @if(isset($prod->imagenesProductos[0]))
                                        <img src="../uploads/{{$prod->imagenesProductos[0]->nombre}}" width="30"
                                            class="img-responsive">
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $productos->links() }}
                </section>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setTimeout(() => {
            $("#mensajeExito").hide();
        }, 2000);
        $(document).on('click', '.btn-eliminar-prod', function () {
            var productoId = $(this).data('producto_id');
            var x = confirm("Estas seguro de Eliminar?");
            if (x)
                $(".formulario" + productoId).submit();
            else
                alert("Cancelaste eliminar");
        });
    });
</script>
@endsection
@extends('layouts.menu')
@section('content')
<div class="container">
    <div class="row">
        <div class="card">
            <div class="card-header bg-info text-white">
                <h1 class="display-5">{{ $productos->nombre }}</h1>
            </div>
            <div class="card-body">
                <section>
                    <h6><strong>Clave:</strong></h6>
                    {{ $productos->clave }}
                    <hr>
                    <h6><strong>Descripción:</strong></h6>
                    {{ $productos->descripcion }}
                    <hr>
                    <h6><strong>Precio:</strong></h6>
                    ${{ $productos->precio }}
                    <hr>
                    <h6><strong>Tipo:</strong></h6>
                    {{$productos->categoriaProducto->nombre}}
                    <hr>
                    <h6><strong>Galería de Imágenes:</strong></h4>

                        <!-- Mostramos todas las imágenes pertenecientes a a este registro -->
                        @foreach($productos->imagenesProductos as $img)
                        <a data-fancybox="gallery" href="../../../uploads/{{ $img->nombre }}">
                            <img src="../../../uploads/{{ $img->nombre }}" width="200" class="img-fluid">
                        </a>
                        @endforeach
                </section>
            </div>
            <div class="card-footer text-center">
                <a href="{{ route('admin.productos.index') }}" class="btn btn-info btn-lg  ml-3">Volver</a>
            </div>
        </div>
    </div>
    @endsection
<div class="row">
  <div class="col-md-12">

    @if ( !empty ( $productos->id) )
    <div class="row">
      <div class="col-xs-6 col-sm-6 col-md-6">
        <div class="form-group">
          <label for="clave" class="font-weight-bold">Clave:</label>
          <div>
            <input class="form-control" placeholder="Ingresa clave" required="required" name="clave" type="text"
              id="clave" value="{{ $productos->clave }}">
          </div>
        </div>
      </div>
      <div class="col-xs-6 col-sm-6 col-md-6">
        <div class="form-group">
          <label for="nombre" class="font-weight-bold">Nombre:</label>
          <div>
            <input class="form-control" placeholder="Ingresa nombre" required="required" name="nombre" type="text"
              id="nombre" value="{{ $productos->nombre }}">
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-6 col-sm-6 col-md-6">
        <div class="form-group">
          <label for="cat_tipo_producto_id" class="font-weight-bold">Tipo:</label>
          <div>
            <select name="cat_tipo_producto_id" id="cat_tipo_producto_id" class="form-control">
              @foreach($categorias as $c)
              @if($c->id == $productos->cat_tipo_producto_id)
              <option value="{{$c->id}}" selected>{{$c->nombre}}</option>
              @else
              <option value="{{$c->id}}">{{$c->nombre}}</option>
              @endif
              @endforeach
            </select>
            <!-- <input class="form-control" placeholder="40" required="required" name="cat_tipo_producto_id" type="text" id="cat_tipo_producto_id" value="{{ $productos->cat_tipo_producto_id }}">                               -->
          </div>
        </div>
      </div>
      <div class="col-xs-6 col-sm-6 col-md-6">
        <div class="form-group">
          <label for="precio" class="font-weight-bold">Precio:</label>
          <div>
            <input class="form-control" placeholder="Ingresa precio" required="required" name="precio" type="text"
              id="precio" value="{{ $productos->precio }}">
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-6 col-sm-6 col-md-6">
        <div class="form-group">
          <label for="descripcion" class="font-weight-bold">Descripción:</label>
          <div>
            <textarea class="form-control" placeholder="Ingrese descripcion del producto" required="required"
              name="descripcion" type="text" id="descripcion"> {{ $productos->descripcion }} </textarea>
          </div>
        </div>
      </div>
      <div class="col-xs-6 col-sm-6 col-md-6">
        <div class="form-group">
          <label for="img" class="font-weight-bold">Selecciona una imagen:</label>
          <div>
            <input name="img[]" type="file" id="img" multiple="multiple">
            <br>
            <br>
          </div>
        </div>
      </div>
      @if ( !empty ( $productos->imagenesProductos) )
      <span> Imagen(es) Actual(es): </span>
      <br>

      <!-- Mensaje: Imagen Eliminada Satisfactoriamente ! -->
      @if(Session::has('message'))
      <div class="alert alert-success" role="alert">
        {{ Session::get('message') }}
      </div>
      @endif
      <!-- Mostramos todas las imágenes pertenecientesa a este registro -->
      @foreach($productos->imagenesProductos as $img)
      <img src="../../../uploads/{{ $img->nombre }}" width="200" class="img-fluid imgProducto{{$img->id}}">
      <!-- Botón para Eliminar la Imagen individualmente -->
      <!-- <a type="button" href="{{ route('admin.productos.eliminarImagen', [$img->id, $productos->id]) }}" class="btn btn-danger btn-sm" onclick="return confirmarEliminar();">Eliminar</a> -->
      <button style="height: min-content;" type="button"
        class=" btn btn-danger botonEliminarImagen imgProducto{{$img->id}}"
        onclick="return confirmarEliminar({{$img->id}});">Eliminar</button>
      @endforeach
      @else
      Aún no se ha cargado una imagen para este producto
      @endif
    </div>

    @else

    <div class="row">
      <div class="col-xs-6 col-sm-6 col-md-6">
        <div class="form-group">
          <label for="clave" class="font-weight-bold">Clave:</label>
          <div>
            <input class="form-control" placeholder="Ingrese clave del producto" required="required" name="clave"
              type="text" id="clave">
          </div>
        </div>
      </div>
      <div class="col-xs-6 col-sm-6 col-md-6">
        <div class="form-group">
          <label for="nombre" class="font-weight-bold">Nombre:</label>
          <div>
            <input class="form-control" placeholder="Ingrese nombre del producto" required="required" name="nombre"
              type="text" id="nombre">
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-6 col-sm-6 col-md-6">
        <div class="form-group">
          <label for="descripcion" class="font-weight-bold">Descripcion:</label>
          <div>
            <textarea class="form-control" placeholder="Ingrese descripcion del producto" required="required"
              name="descripcion" type="text" id="descripcion">
                    </textarea>
          </div>
        </div>
      </div>
      <div class="col-xs-6 col-sm-6 col-md-6">
        <div class="form-group">
          <label for="precio" class="font-weight-bold">Precio:</label>
          <div>
            <input class="form-control" placeholder="Ingrese el precio del producto" required="required" name="precio"
              type="text" id="precio">
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-6 col-sm-6 col-md-6">
        <div class="form-group">
          <label for="cat_tipo_producto_id" class="font-weight-bold">Categoría:</label>
          <div>
            <select name="cat_tipo_producto_id" id="cat_tipo_producto_id" class="form-control">
              @foreach($categorias as $c)
              <option value="{{$c->id}}">{{$c->nombre}}</option>
              @endforeach
            </select>
            <!-- <input class="form-control" placeholder="Ingrese el cat_tipo_producto_id del producto" required="required" name="cat_tipo_producto_id" type="text" id="cat_tipo_producto_id">                               -->
          </div>
        </div>
      </div>
      <div class="col-xs-6 col-sm-6 col-md-6">
        <div class="form-group">
          <label for="img" class="font-weight-bold">Selecciona una imagen:</label>
          <div>
            <input name="img[]" type="file" id="img" multiple="multiple" required>
          </div>
        </div>
      </div>
    </div>
    @endif
    <div class="text-right mt-4">
      <button type="submit" class="btn btn-info">Guardar</button>
      <a href="" class="btn btn-warning">Cancelar</a>
    </div>
  </div>
</div>
@extends('layouts.menu')
@section('content')

<div class="container">
  <div class="row ">
    <div class="col align-self-end">
      <div class="card">
        <div class="card-header bg-info text-white ">
          <h4 class="display-5">Editar: {{$productos->nombre}}</h4>
        </div>
        <div class="card-body">
          <section class="example">
            <div class="alert alert-success" role="alert" id="mensajeEliminarImg" style="display:none">
              "Se eliminó correctamente la imagen"
            </div>
            <form method="POST" action="{{ route('admin.productos.update',$productos->id) }}" role="form"
              enctype="multipart/form-data">
              <input type="hidden" name="_method" value="PUT">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              @include('admin.productos.frm.form')
            </form>
          </section>
        </div>
        <div class="card-footer text-center">
          <a href="{{ route('admin.productos.index') }}" class="btn btn-success ml-3">Volver</a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
<script type="text/javascript">

  function confirmarEliminar(imgId) {
    var route = "{{route("admin.productos.eliminarImagen")}}";
    var x = confirm("Eliminar esta imagen ?");

    if (x)
      // return true;
      $.ajax({
        type: 'POST', //tipo de peticion
        url: route, //direccion de la peticion url
        data: {
          imgId: imgId,
          _token: '{{csrf_token()}}',
        }, //paramentros a enviar por la peticion
        dataType: 'json',
        success: function (respuestaAjax) {
          $("#mensajeEliminarImg").show();
          // alert('Se elimino correctamente');
          $(".imgProducto" + imgId).remove();
          setTimeout(() => {
            $("#mensajeEliminarImg").hide();
          }, 5000);
        },
        error: function (err) {
          alert('hubo un error en la petición');
          alert(err.status);
        }
      });

    else
      return false;
  }




</script>

@endsection
@extends('layouts.menu')
@section('content')
        <div class="row justify-content-center align-items-center py-5">
                  <h2 >{{ config('textos.clientes') }}</h2>
        </div>
        <div class="row justify-content-center align-items-center py-2">            
                @if ($data->count())
                <table class="table table-borderless table-hover">
                    <thead class="thead-dark">
                      <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Usuarios</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $item)
                        <tr>
                            <th scope="row">{{ $item->id }}  </th>
                            <td><a href="{{action('UsuariosController@edit', $item->id)}}" class="list-group-item list-group-item-action"> {{ $item->email }}</a></td>
                          </tr>
                        @endforeach
                    </tbody>
                  </table>
            @else
                <a class="list-group-item list-group-item-action">
                    {{ config('textos.nohay') }}
                </a>
            @endif
        </div>
@endsection

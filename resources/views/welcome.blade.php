@extends('layouts.menu')
@section('content')
<div class="container">
    @if (session('status'))
    <div class="alert alert-success" role="alert">
        <h3>{{session('status')}}</h3>
    </div>
         
    @endif
    <div class="row ">
   
        <div>
            <h1>Productos</h1> 
        </div>
        <hr>
        <br>
        <br>
        <br>
        <div class="card-columns">
            @foreach($productos as $prod)
            <div class="col mb-4">
                <div class="card">
                    <img src="{{url('/')}}/uploads/{{$prod->imagenesProductos[0]->nombre}}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-title">{{$prod->nombre}}</p>
                        <h6 class="card-text">$ {{$prod->precio}}</h6>
                        <button class="btn btn-info btn-block ver_detalles" data-toggle="modal"
                            data-target="#VerDatosModal" data-id-prod="{{$prod->id}}" data-clave="{{$prod->clave}}"
                            data-nombre="{{$prod->nombre}}" data-precio="{{$prod->precio}}"
                            data-descripcion="{{$prod->descripcion}}" data-img="{{$prod->imagenesProductos}}">Ver
                            detalles</button>
                        <a class="btn btn-warning btn-block agregar_producto"
                            data-id-producto="{{$prod->id}}">Añadir</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>


        <!-- Modal detalles -->
        <div class="modal fade" id="VerDatosModal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 text-center" id="imagen">
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <h5><strong><span id="nombre"></span></strong></h5>
                                        <p>Código: <span id="clave"></span></p>
                                        <hr>
                                        <h4>$<span id="precio"></span></h4>
                                        <p><span id="descripcion"></span></p>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-xs-6 col-sm-6 col-md-2">
                                                        <p>Cantidad:</p>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-2">
                                                        <select class="form-control" id="cantidad_producto">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-">
                                                        <p class="text-info">Límite: 6</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br><br>
                                        <button class="btn btn-info btn-lg agregar_producto_modal"
                                            id="agregarModal">Agregar al carrito</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="modal-footer md-4">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>


@endsection

@section('js')


<script type="text/javascript">

    $(document).ready(function () {

        $(document).on('click', '.ver_detalles', function () {
            $("#imagen").html("");
            let id = $(this).attr('data-id-prod');
            let clave = $(this).attr('data-clave');
            let nombre = $(this).attr('data-nombre');
            let precio = $(this).attr('data-precio');
            let descripcion = $(this).attr('data-descripcion');
            let img = $(this).attr('data-img');
            $('#clave').html(clave);
            $('#nombre').html(nombre);
            $('#precio').html(precio);
            $('#descripcion').html(descripcion);
            $('#img').html(img);
            $('#agregarModal').attr('data-id-producto', id);
            console.log(img);
            $.each(JSON.parse(img), function (index, valor) {
                $('#imagen').append(

                    // "<img src=\"/uploads/" + valor.nombre + "\" alt=\"...\" width=\"70%\">"
                    '<img src="{{url('/')}}/uploads/'+valor.nombre+'" alt="..." width="70%">'
                );
            });

        })

        $(document).on('click', '.agregar_producto', function () {
            let id_producto = $(this).attr('data-id-producto');

            agregarProducto(id_producto);
        });

        $(document).on('click', '.agregar_producto_modal', function () {
            let id_producto = $(this).attr('data-id-producto');
            let cant_producto = $('#cantidad_producto').val();
            agregarProducto(id_producto, cant_producto);
        });

    })

    function agregarProducto(id_producto, cantidad = 1) {
        var route = "{{route("agregar.producto")}}";
        $.ajax({
            type: 'POST', //tipo de peticion
            url: route, //direccion de la peticion url
            data: {
                id_producto: id_producto,
                cantidad: cantidad,
                _token: '{{csrf_token()}}',
            }, //paramentros a enviar por la peticion
            dataType: 'json',
            success: function (respuestaAjax) {
                alert('Se añadio producto al carrito');
            },
            error: function (err) {
                alert('hubo un error en la petición');
                alert(err.status);
            }
        });
    }

</script>

@endsection
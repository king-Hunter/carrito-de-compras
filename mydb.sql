-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-02-2021 a las 15:33:07
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mydb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrito`
--

CREATE TABLE `carrito` (
  `id` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `cantidad` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `carrito`
--

INSERT INTO `carrito` (`id`, `fecha`, `cantidad`, `users_id`, `producto_id`) VALUES
(1, '2021-02-13 22:14:58', 1, 4, 40);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_tipo_producto`
--

CREATE TABLE `cat_tipo_producto` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cat_tipo_producto`
--

INSERT INTO `cat_tipo_producto` (`id`, `nombre`) VALUES
(1, 'Aprendizaje'),
(2, 'Bebés'),
(3, 'Figuras de acción'),
(4, 'Juegos de mesa'),
(5, 'Para construir'),
(6, 'Peluches'),
(7, 'Muñecas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `id` int(11) NOT NULL,
  `fecha_compra` datetime NOT NULL,
  `monto` decimal(10,0) NOT NULL,
  `folio` varchar(45) NOT NULL,
  `users_id` int(11) NOT NULL,
  `direccion_usuario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `compras`
--

INSERT INTO `compras` (`id`, `fecha_compra`, `monto`, `folio`, `users_id`, `direccion_usuario_id`) VALUES
(1, '2021-02-12 00:25:09', '2000', 'A0001', 2, 1),
(2, '2021-02-12 00:25:09', '3000', 'A0002', 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compra_producto`
--

CREATE TABLE `compra_producto` (
  `precio_compra` decimal(10,0) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `compra_id` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `compra_producto`
--

INSERT INTO `compra_producto` (`precio_compra`, `cantidad`, `compra_id`, `producto_id`) VALUES
('589', 1, 1, 37);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compra_status`
--

CREATE TABLE `compra_status` (
  `id` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `compra_id` int(11) NOT NULL,
  `nombre_status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `compra_status`
--

INSERT INTO `compra_status` (`id`, `fecha`, `compra_id`, `nombre_status`) VALUES
(1, '2021-02-13 10:30:13', 1, 'Entregado'),
(2, '2021-02-13 10:30:38', 2, 'Enviado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos_usuario`
--

CREATE TABLE `datos_usuario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(150) DEFAULT NULL,
  `paterno` varchar(150) DEFAULT NULL,
  `materno` varchar(150) DEFAULT NULL,
  `telefono` varchar(25) DEFAULT NULL,
  `users_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `datos_usuario`
--

INSERT INTO `datos_usuario` (`id`, `nombre`, `paterno`, `materno`, `telefono`, `users_id`) VALUES
(1, 'Shaila', 'Palafox', 'Hernaández', '2411001063', 1),
(2, 'Shaila', 'Palafox', 'Hernández', '2411001063', 2),
(3, NULL, NULL, NULL, NULL, 3),
(4, NULL, NULL, NULL, NULL, 4),
(5, 'Ross', 'Hernandez', 'Garcia', '(222) 111-1116', 2),
(6, 'Pepe', 'Ponce', 'Ramirez', '2231110010', 4),
(7, 'Ross', 'Hernandez', 'Garcia', '(222) 111-1115', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direccion_usuario`
--

CREATE TABLE `direccion_usuario` (
  `id` int(11) NOT NULL,
  `calle` varchar(45) NOT NULL,
  `numero_ext` varchar(45) NOT NULL,
  `numero_int` varchar(45) DEFAULT NULL,
  `referencia` varchar(45) DEFAULT NULL,
  `colonia` varchar(45) NOT NULL,
  `municipio` varchar(45) NOT NULL,
  `estado` varchar(45) NOT NULL,
  `codigo_postal` varchar(6) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `datos_usuario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `direccion_usuario`
--

INSERT INTO `direccion_usuario` (`id`, `calle`, `numero_ext`, `numero_int`, `referencia`, `colonia`, `municipio`, `estado`, `codigo_postal`, `status`, `datos_usuario_id`) VALUES
(1, 'La misma', '122', '123', 'Por aqui', 'Colonia', 'Nose ', 'Tlax', '33421', 0, 1),
(2, 'Por aqui y alla', '321', '322', 'cerquita', 'Colon', 'Tlax', 'Tlax', '12343', 0, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `img_productos`
--

CREATE TABLE `img_productos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `formato` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `producto_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `img_productos`
--

INSERT INTO `img_productos` (`id`, `nombre`, `formato`, `producto_id`, `created_at`, `updated_at`) VALUES
(37, 'default_ujd8tebygkmoukkn.jpg', 'jpg', 37, NULL, NULL),
(38, '34957_oo1gjnkqw3nqgzpr.jpg', 'jpg', 38, NULL, NULL),
(39, 'b2176_vrjbfvjbxt1imrsn.jpg', 'jpg', 39, NULL, NULL),
(40, 'ghx78-1_9hhinrgkijvsnkga.jpg', 'jpg', 40, NULL, NULL),
(41, 'ghx78-2_ugpi26jaqbstffgs.jpg', 'jpg', 40, NULL, NULL),
(42, '31058-box1-v39_qzgr5hy902o0vviu.jpg', 'jpg', 41, NULL, NULL),
(43, '31058-box5-v39_ysgkk8akptm4dt5h.jpg', 'jpg', 41, NULL, NULL),
(44, 'cmy09-1_n1igkt4tiir6hzke.jpg', 'jpg', 42, NULL, NULL),
(45, 'cmy09-2_as1jfxreilk0xgkr.jpg', 'jpg', 42, NULL, NULL),
(56, 'cat.jpg', 'jpg', 50, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2021_02_07_162351_create_img_productos_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('admin@admin.com', '$2y$10$d4QcdUTJOg36yOZ4B6unyO3uGisewn0da/8MMlMsbW4NW32nekStC', '2021-02-11 03:33:17'),
('shaila.ph@hotmail.com', '$2y$10$Q6.OgqVL9ZArAjqxW3foZOB3fKe22t6BsCpA2jGu0.A8OoQmAdV8a', '2021-02-12 06:06:33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id` int(11) NOT NULL,
  `clave` varchar(45) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `descripcion` varchar(500) DEFAULT NULL,
  `precio` decimal(10,0) NOT NULL,
  `cat_tipo_producto_id` int(11) NOT NULL,
  `imagenes` int(11) NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `clave`, `nombre`, `descripcion`, `precio`, `cat_tipo_producto_id`, `imagenes`, `url`) VALUES
(37, '11956052813', 'Oso Pequeño Chocolate', 'Toda La Ternura De Estos Adorables Y Populares Personajes Llegó A Gund.', '389', 6, 20210212, 'oso-pequeno-chocolate'),
(38, '502134957', 'Funko POP! Fortnite: Rex', 'Funko POP! Fortnite: Rex', '299', 3, 20210212, 'funko-pop-fortnite-rex'),
(39, '1152B2176', 'Hasbro Gaming B2176 Operando Clásico', '¡Sana a Sam y evita sus quejidos! Utiliza las pinzas para sacar cuidadosamente las 12 divertidas partes de su cuerpo.', '699', 4, 20210212, 'hasbro-gaming-b2176-operando-clasico'),
(40, '1005GHX78', 'Fisher-Price Zorrito Primeros Gateos', 'El Zorrito Primeros Gateos de Fisher-Price® mantiene a los bebés ocupados con el aprendizaje musical divertido a medida que crecen: los acompaña desde que juegan sentados hasta que gatean por aquí, por allá y en todas partes.', '679', 2, 20210212, 'fisher-price-zorrito-primeros-gateos'),
(50, '1234567', 'Pirma', 'Bonitos', '1000', 2, 20210215, 'pirma');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `tipo_usuario` tinyint(4) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `tipo_usuario`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin@admin.com', '$2y$10$T8eVkN74hW6vJ8nDJ9NhieMTF/rIYGc0KeAInCfhZTtvxKfmGiT4.', 1, '5PIdmRQ3VjGCvIjmhHsa4utSshwgXW73BeuncguMP6OZZFRkk8KF28hga9Kp', '2021-02-07 13:10:06', '2021-02-07 13:10:06'),
(2, 'shaila.ph@hotmail.com', '$2y$10$P8UUJvh7QshpVB8IcAHwzuFgad/Tcc3Hmngz4wMYbnyaJboa4.Dxu', 0, 'KxmvANHQMehorWMN4rAJENVssQUWhLklA10NsLbVDwCQSTP8P5f6TGrXtuVu', '2021-02-12 06:05:42', '2021-02-12 06:05:42'),
(3, 'ross@admin.com', '$2y$10$uugNDTEFyl6tJK1CoIZ2lOxqa1H6m5afM1VGSkLQqS6kbnYXVUuIS', 1, 'URReJOfH9l7cDYKiSlOsdr4pEnwkLbf5CTVZY5e6de4hSI24dTxMQK28CKuM', '2021-02-13 12:12:59', '2021-02-13 12:12:59'),
(4, 'ross@user.com', '$2y$10$Wp50G5fg9SOsgkzw2I1JJ.ZvrdHpfV3/8s8fkwYwmPPYJqHmw007G', 0, 'T6nknnmdZLGPK2mWG1BQdKvpF8vPJZIQkbur66hZWYqz1ohpv5xD1n2UNUFw', '2021-02-13 12:13:28', '2021-02-13 12:13:28'),
(5, 'ross@user2.com', '$2y$10$lQNfkruW4s3wOwmdvpGN0OK.i9R77XkH5jbzhgIlROcOXa.T.fkoe', 0, 'xiksvz29MWab5hjhMTq6VwqfMMDTlkFlA8nG35vTMv97cgerHlzOrK8QNYgg', '2021-02-15 10:55:41', '2021-02-15 10:55:41');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `carrito`
--
ALTER TABLE `carrito`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_carrito_users1_idx` (`users_id`),
  ADD KEY `fk_carrito_producto1_idx` (`producto_id`);

--
-- Indices de la tabla `cat_tipo_producto`
--
ALTER TABLE `cat_tipo_producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_compra_users1_idx` (`users_id`),
  ADD KEY `fk_compra_direccion_usuario1_idx` (`direccion_usuario_id`);

--
-- Indices de la tabla `compra_producto`
--
ALTER TABLE `compra_producto`
  ADD KEY `fk_compra_producto_compra1_idx` (`compra_id`),
  ADD KEY `fk_compra_producto_producto1_idx` (`producto_id`);

--
-- Indices de la tabla `compra_status`
--
ALTER TABLE `compra_status`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_compra_status_compra1_idx` (`compra_id`);

--
-- Indices de la tabla `datos_usuario`
--
ALTER TABLE `datos_usuario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_datos_usuario_users_idx` (`users_id`);

--
-- Indices de la tabla `direccion_usuario`
--
ALTER TABLE `direccion_usuario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_direccion_usuario_datos_usuario1_idx` (`datos_usuario_id`);

--
-- Indices de la tabla `img_productos`
--
ALTER TABLE `img_productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_producto_cat_tipo_producto1` (`cat_tipo_producto_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `carrito`
--
ALTER TABLE `carrito`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `cat_tipo_producto`
--
ALTER TABLE `cat_tipo_producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `compra_status`
--
ALTER TABLE `compra_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `datos_usuario`
--
ALTER TABLE `datos_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `direccion_usuario`
--
ALTER TABLE `direccion_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `img_productos`
--
ALTER TABLE `img_productos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `carrito`
--
ALTER TABLE `carrito`
  ADD CONSTRAINT `fk_carrito_producto1` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_carrito_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `compras`
--
ALTER TABLE `compras`
  ADD CONSTRAINT `fk_compra_direccion_usuario1` FOREIGN KEY (`direccion_usuario_id`) REFERENCES `direccion_usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_compra_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `compra_producto`
--
ALTER TABLE `compra_producto`
  ADD CONSTRAINT `fk_compra_producto_compra1` FOREIGN KEY (`compra_id`) REFERENCES `compras` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_compra_producto_producto1` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `compra_status`
--
ALTER TABLE `compra_status`
  ADD CONSTRAINT `fk_compra_status_compra1` FOREIGN KEY (`compra_id`) REFERENCES `compras` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `datos_usuario`
--
ALTER TABLE `datos_usuario`
  ADD CONSTRAINT `fk_datos_usuario_users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `direccion_usuario`
--
ALTER TABLE `direccion_usuario`
  ADD CONSTRAINT `fk_direccion_usuario_datos_usuario1` FOREIGN KEY (`datos_usuario_id`) REFERENCES `datos_usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `fk_producto_cat_tipo_producto1` FOREIGN KEY (`cat_tipo_producto_id`) REFERENCES `cat_tipo_producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
